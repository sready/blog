title: Vidalia Logo Design Contest
---
pub_date: 2008-08-16
---
author: phobos
---
tags:

vidalia
logo
contest
---
categories: applications
---
_html_body:

<p>We are currently sponsoring a <a href="http://www.worth1000.com/contest.asp?contest_id=20680" rel="nofollow">design contest</a> to create a new logo for <a href="http://www.vidalia-project.net" rel="nofollow">Vidalia</a>. The winner of the contest will receive a $250 USD cash prize. The firm deadline for contest submissions is August 22, 2008.</p>

<p>The logo will be used in the Vidalia software and related installers, on the website, and on t-shirts. Designers are free to choose any fonts, color combinations, and symbol options you like (no onions, though, please). The logo must include a symbolic component that is recognizable by itself without the name "Vidalia" next to it. See the <a rel="nofollow">contest page</a> for more details. If you have further questions, please email <a href="mailto:contest@vidalia-project.net" rel="nofollow">contest@vidalia-project.net</a> or stop by #vidalia on irc.oftc.net.</p>

<p>Here's the overall timeline for the contest:</p>

<ul>
<li>
    <b>August 15 – August 22:</b> Entries may be submitted at the <a href="http://www.worth1000.com/contest.asp?contest_id=20680" rel="nofollow">Worth1000 contest page</a>.
  </li>
<li>
    <b>August 23 – August 24:</b> Everyone is welcome to review the submissions received and vote on their favorite design. Even if you didn't submit anything, you can still vote!
  </li>
<li>
    <b>August 25 – August 31:</b> The final winner will be announced by August 31 at the latest.
  </li>
</ul>

<p>Late entries will not be eligible for the cash prize, so be sure to get your<br />
submission in by August 22!</p>

---
_comments:

<a id="comment-186"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-186" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 16, 2008</p>
    </div>
    <a href="#comment-186">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-186" class="permalink" rel="bookmark">Design Contests are Spec Work</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="http://www.no-spec.com/" rel="nofollow">And spec work is a bad thing</a>.</p>
<p>I hope whoever is behind running the contest considers reading that site and following the advice put forth.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-187"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-187" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="http://www.cnn.com">Anonymous (not verified)</a> said:</p>
      <p class="date-time">August 17, 2008</p>
    </div>
    <a href="#comment-187">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-187" class="permalink" rel="bookmark">Off-topic suggestion</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Off-topic suggestion:</p>
<p>Adding some of this somewhere in Tor's documentation or Wik could be helpful.</p>
<p>    *  Use Firefox Portable for Tor only sessions.<br />
    * Go through the preferences and disable anything that logs or leaks information. This includes, but is not limited to:<br />
          o In "Content," disable java/javascript.<br />
          o In "Privacy," remember nothing. Remember no history. Don't accept cookies, no exceptions. Always clear everything before closing.<br />
          o In "Security," warn before installing addons. Remove all exceptions. Remember no passwords.<br />
          o In "Advanced" - Network, set the cache size to 0.<br />
          o In "Advanced" - Update, disable auto updating/checking for updates.<br />
    * Install the Torbutton extension - the one that overhauls browser security, not just change proxy settings. The latest version at this time is 1.2.0.<br />
          o Enable every check mark.<br />
          o Clear cookies on any browser shutdown (but cookies should be disabled!).<br />
          o Clear cookies on Tor toggle ("").<br />
          o Whenever there's an option to choose between Tor and non-Tor, choose Tor.<br />
          o Block tor disk cache, but you'll want to allow memory cache to prevent redownloading of images.<br />
          o Make sure the proxy settings point to privoxy.<br />
    * Set network.http.sendRefererHeader to 0 in about:config. Also set false for network.http.sendSecureXSiteReferrer, or use Refcontrol.<br />
    * In the proxy settings make sure the proxy exceptions list is empty. It can be used as a way for sites to access localhost.<br />
    * One annoying thing about Firefox is the way it handles external applications. You want to disable them all because sites may call telnet for example, which can leak your IP address among other things. Look in about:config for network.protocol-handler.external. You'll want to set the default and all the subsettings to false. Then look for network.protocol-handler.warn-external. You want to set the default and all the subsettings to true. Is there an extension that does this?<br />
    * Firefox checks for extensions without the user's consent; a temporary solution is to set extensions.blocklist.enabled to false.<br />
    * There are probably other information leaks that could come about if you accidentally click a button. Search through about:config for URLs and blank them out. The types of links I've seen are http, https, and rdp. Don't remove anything with chrome:// or resource:// though.<br />
    * Make sure the plugin directory is empty. Do this again every time you update Flash or Shockwave.<br />
    * Set the home page to about:blank </p>
<p>Because of the huge amount of lag associated with Tor, pipelining, i.e. sending http requests in batches, makes a lot of sense. Go to about:config and make the following changes: network.http.keep-alive.timeout:600 network.http.max-persistent-connections-per-proxy:16 network.http.pipelining:true network.http.pipelining.maxrequests:8 network.http.proxy.keep-alive:true network.http.proxy.pipelining:true</p>
<p>You should also disable prefetching. This is when the browser tries to predict what the user will click next. It is a waste of Tor bandwidth, and should be disabled by setting network.prefetch-next to false.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-3623"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3623" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 18, 2009</p>
    </div>
    <a href="#comment-3623">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3623" class="permalink" rel="bookmark">I still find Drupal</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I still find Drupal templates sites are lagging behind the number of Joomla template sites. The more templates available free or otherwise will help the popularity of Drupal.</p>
</div>
  </div>
</article>
<!-- Comment END -->
