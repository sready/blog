title: Tor 0.3.2.3-alpha is released, with small bugfixes
---
pub_date: 2017-10-27
---
author: nickm
---
tags: alpha release
---
categories: releases
---
summary: Tor 0.3.2.3-alpha is the third release in the 0.3.2 series. It fixes numerous small bugs in earlier versions of 0.3.2.x, and adds a new directory authority, Bastet.
---
_html_body:

<p>Tor 0.3.2.3-alpha is the third release in the 0.3.2 series. It fixes numerous small bugs in earlier versions of 0.3.2.x, and adds a new directory authority, Bastet.</p>
<p>You can download the source from the usual place on the website. Binary packages should be available soon, with an alpha Tor Browser likely some time in November.</p>
<p>Remember: This is an alpha release, and it's likely to have more bugs than usual. We hope that people will try it out to find and report bugs, though.</p>
<h2>Changes in version 0.3.2.3-alpha - 2017-10-27</h2>
<ul>
<li>Directory authority changes:
<ul>
<li>Add "Bastet" as a ninth directory authority to the default list. Closes ticket <a href="https://bugs.torproject.org/23910">23910</a>.</li>
<li>The directory authority "Longclaw" has changed its IP address. Closes ticket <a href="https://bugs.torproject.org/23592">23592</a>.</li>
</ul>
</li>
<li>Minor features (bridge):
<ul>
<li>Bridge relays can now set the BridgeDistribution config option to add a "bridge-distribution-request" line to their bridge descriptor, which tells BridgeDB how they'd like their bridge address to be given out. (Note that as of Oct 2017, BridgeDB does not yet implement this feature.) As a side benefit, this feature provides a way to distinguish bridge descriptors from non-bridge descriptors. Implements tickets 18329.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (client, entry guards):
<ul>
<li>Improve log messages when missing descriptors for primary guards. Resolves ticket <a href="https://bugs.torproject.org/23670">23670</a>.</li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the October 4 2017 Maxmind GeoLite2 Country database.</li>
</ul>
</li>
<li>Minor bugfixes (bridge):
<ul>
<li>Overwrite the bridge address earlier in the process of retrieving its descriptor, to make sure we reach it on the configured address. Fixes bug <a href="https://bugs.torproject.org/20532">20532</a>; bugfix on 0.2.0.10-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (documentation):
<ul>
<li>Document better how to read gcov, and what our gcov postprocessing scripts do. Fixes bug <a href="https://bugs.torproject.org/23739">23739</a>; bugfix on 0.2.9.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (entry guards):
<ul>
<li>Tor now updates its guard state when it reads a consensus regardless of whether it's missing descriptors. That makes tor use its primary guards to fetch descriptors in some edge cases where it would previously have used fallback directories. Fixes bug <a href="https://bugs.torproject.org/23862">23862</a>; bugfix on 0.3.0.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (onion service client):
<ul>
<li>When handling multiple SOCKS request for the same .onion address, only fetch the service descriptor once.</li>
<li>When a descriptor fetch fails with a non-recoverable error, close all pending SOCKS requests for that .onion. Fixes bug <a href="https://bugs.torproject.org/23653">23653</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (onion service):
<ul>
<li>Always regenerate missing onion service public key files. Prior to this, if the public key was deleted from disk, it wouldn't get recreated. Fixes bug <a href="https://bugs.torproject.org/23748">23748</a>; bugfix on 0.3.2.2-alpha. Patch from "cathugger".</li>
<li>Make sure that we have a usable ed25519 key when the intro point relay supports ed25519 link authentication. Fixes bug <a href="https://bugs.torproject.org/24002">24002</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (onion service, v2):
<ul>
<li>When reloading configured onion services, copy all information from the old service object. Previously, some data was omitted, causing delays in descriptor upload, and other bugs. Fixes bug <a href="https://bugs.torproject.org/23790">23790</a>; bugfix on 0.2.1.9-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (memory safety, defensive programming):
<ul>
<li>Clear the target address when node_get_prim_orport() returns early. Fixes bug <a href="https://bugs.torproject.org/23874">23874</a>; bugfix on 0.2.8.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (relay):
<ul>
<li>Avoid a BUG warning when receiving a dubious CREATE cell while an option transition is in progress. Fixes bug <a href="https://bugs.torproject.org/23952">23952</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (testing):
<ul>
<li>Adjust the GitLab CI configuration to more closely match that of Travis CI. Fixes bug <a href="https://bugs.torproject.org/23757">23757</a>; bugfix on 0.3.2.2-alpha.</li>
<li>Prevent scripts/test/coverage from attempting to move gcov output to the root directory. Fixes bug <a href="https://bugs.torproject.org/23741">23741</a>; bugfix on 0.2.5.1-alpha.</li>
<li>When running unit tests as root, skip a test that would fail because it expects a permissions error. This affects some continuous integration setups. Fixes bug <a href="https://bugs.torproject.org/23758">23758</a>; bugfix on 0.3.2.2-alpha.</li>
<li>Stop unconditionally mirroring the tor repository in GitLab CI. This prevented developers from enabling GitLab CI on master. Fixes bug <a href="https://bugs.torproject.org/23755">23755</a>; bugfix on 0.3.2.2-alpha.</li>
<li>Fix the onion service v3 descriptor decoding fuzzing to use the latest decoding API correctly. Fixes bug <a href="https://bugs.torproject.org/21509">21509</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (warnings):
<ul>
<li>When we get an HTTP request on a SOCKS port, tell the user about the new HTTPTunnelPort option. Previously, we would give a "Tor is not an HTTP Proxy" message, which stopped being true when HTTPTunnelPort was introduced. Fixes bug <a href="https://bugs.torproject.org/23678">23678</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-272218"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272218" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>someone (not verified)</span> said:</p>
      <p class="date-time">October 29, 2017</p>
    </div>
    <a href="#comment-272218">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272218" class="permalink" rel="bookmark">When we will see tor.exe 64…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When we will see tor.exe 64-bit for Windows?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272229"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272229" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>joker (not verified)</span> said:</p>
      <p class="date-time">October 30, 2017</p>
    </div>
    <a href="#comment-272229">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272229" class="permalink" rel="bookmark">Thank you</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272264"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272264" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>some2 (not verified)</span> said:</p>
      <p class="date-time">October 31, 2017</p>
    </div>
    <a href="#comment-272264">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272264" class="permalink" rel="bookmark">@someone: For what exactly…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>@someone: For what exactly do you need a 64-bit windows version? Just use 32-bit...WoW64...  And,well, to get an exe... compile it :-) I usually use mingw32/msys, but your mileage may vary. IMHO, if you cannot compile it yourself, you shouldn't be using tor alpha, in your very own interest - no offense, but think about it, please :-)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272265"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272265" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>yours sincerely (not verified)</span> said:</p>
      <p class="date-time">October 31, 2017</p>
    </div>
    <a href="#comment-272265">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272265" class="permalink" rel="bookmark">Tor should keep have kept on…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor should keep have kept on supporting Windows XP</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272282"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272282" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 02, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-272265" class="permalink" rel="bookmark">Tor should keep have kept on…</a> by <span>yours sincerely (not verified)</span></p>
    <a href="#comment-272282">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272282" class="permalink" rel="bookmark">+1</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>+1</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-272269"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272269" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>isp blockers (not verified)</span> said:</p>
      <p class="date-time">November 01, 2017</p>
    </div>
    <a href="#comment-272269">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272269" class="permalink" rel="bookmark">strange isp blocking tor now…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>strange isp blocking tor now. can't even get through unless you vpn or proxy</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272281"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272281" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>robcio (not verified)</span> said:</p>
      <p class="date-time">November 02, 2017</p>
    </div>
    <a href="#comment-272281">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272281" class="permalink" rel="bookmark">witam  i dzieki :D</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>witam  i dzieki :D</p>
</div>
  </div>
</article>
<!-- Comment END -->
