title: Coming up in Tor 0.2.6...
---
pub_date: 2015-03-03
---
author: nickm
---
tags:

development
0.2.6
features
tech
---
categories:

devops
releases
---
_html_body:

<p>Hi, all!  We've begun the <a href="https://lists.torproject.org/pipermail/tor-dev/2015-January/008216.html" rel="nofollow">feature-freeze</a> for Tor 0.2.6, so we know with pretty high probability what new features and changes will be in the next stable release.  The <a href="https://gitweb.torproject.org/tor.git/tree/ChangeLog" rel="nofollow">ChangeLog</a> and ReleaseNotes files will have a pretty comprehensive overview of what changed since 0.2.5, but I thought I'd like to write up some better descriptions of the major changes.</p>

<h3>AF_UNIX socket support for clients and hidden services</h3>

<p>This is going to help developers of applications that use Tor make their designs more secure.  On Unix and OS X, applications can bind to objects in the file namespace and use them as if they were network connections.  Now, Tor can receive connections and make connections to local hidden service applications over this mechanism.  That's pretty cool, because it means that integrators can prevent other applications from using the network at all, to ensure that they never make connections that don't go through Tor.  Maybe we could use this eventually to turn off networking in Tor Browser entirely on OSX and Unix.</p>

<p>(Sorry, no Windows NamedPipe support yet.  That would be cool for a future version, but it'll require some major hacks in our network stack.)</p>

<p>Jake, Andrea, David and I have all worked on the implementation here; it ended up being pretty elegant.</p>

<p>More info: <a href="https://trac.torproject.org/projects/tor/ticket/12585" rel="nofollow">#12585</a>, <a href="https://trac.torproject.org/projects/tor/ticket/11485" rel="nofollow">#11485</a>, <a href="https://trac.torproject.org/projects/tor/ticket/14451" rel="nofollow">#14451</a>.</p>

<h3>Faster cpuworker design</h3>

<p>We want to use multithreading to spread Tor's cryptography across more cores, but one challenge in doing so has been that our old code for dividing work across multiple cores was written 12 years ago, and designed to accommodate systems that didn't support threading at all.  To support no-threading builds, we allowed the CPU workers to be processes instead of threads, and we always communicated with workers over a socket pair.</p>

<p>No-threading systems are no longer relevant, though, and we can totally do better now.  We now have a nice simple condition-and-queue based thread pool.</p>

<p>More info: <a href="https://trac.torproject.org/projects/tor/ticket/9682" rel="nofollow">#9682</a>.</p>

<h3>Much more testing</h3>

<p>In 0.2.5, we introduced a practice of requiring high test coverage for all new or modified code whenever possible.  This is continuing to bear fruit for us: We've had fewer weird undiagnosable bugs around this time than previously.</p>

<p>We've also added more external testing: The "make test-stem" target can run the Stem unit tests on Tor to make sure they pass.</p>

<p>Also, a few volunteers have stepped up to contribute to our testing infrastructure.  Now test-networks can start in seconds, not minutes.  This has already encouraged me to run the network tests more frequently.</p>

<h3>Improved circuit scheduler</h3>

<p>In older versions of Tor, we used a priority-based scheduler on each connection to decide which circuits to relay traffic for.  But connections were scheduled individually rather than globally.  That meant that even though the most appropriate circuit got attention on each connection, we might write to connections that did not contain the most important circuit.</p>

<p>(To improve latency, we prefer circuits that have not sent many cells recently.)</p>

<p>Now, thanks to ticket <a href="https://trac.torproject.org/projects/tor/ticket/9262" rel="nofollow">#9262</a>, we have an improved, "global" circuit scheduler.  Tor relays now consider all circuits on all connections that are ready to write at once.  This is not expected to yield immediate performance improvements, but once the <a href="https://www.usenix.org/conference/usenixsecurity14/technical-sessions/presentation/jansen" rel="nofollow">KIST scheduler</a> is deployed, it should lead to lower latency for more connections.</p>

<p>(Kudos go to Andrea for her software engineering skills here: since we've merged it, it has actually '''worked''.  That's impressive for a thing of this complexity.)</p>

<p>More info: <a href="https://trac.torproject.org/projects/tor/ticket/9262" rel="nofollow">#9262</a></p>

<h3>Even better support for AutomapHostsOnResolve</h3>

<p>We've got a cool feature "AutomapHostsOnResolve feature" for DNSPort users where, when you ask Tor to resolve an address that doesn't have an IP (such as for a hidden service), it can instead give you a temporary IP address (like 127.192.34.56 or fcf0:1234:9312:1111:2222:9803:1bda:20ef), and treat later connections to that address as if they were to the original address.</p>

<p>This feature has had some longstanding warts, though.  It didn't always work with IPv6; it interacted weirdly with MapAddress, and it tended to reset itself if you changed the configuration.  Worse, it was in one of our messiest functions, so making changes to it was a bit difficult.</p>

<p>In 0.2.6, we've fixed that code (!) I hope, made the feature more robust, cleaned up the function that implements it, and written a bunch of tests for it to help development in the future.</p>

<p>More info: <a href="https://trac.torproject.org/projects/tor/ticket/7555" rel="nofollow">#7555</a>, <a href="https://trac.torproject.org/projects/tor/ticket/12509" rel="nofollow">#12509, </a><a href="https://trac.torproject.org/projects/tor/ticket/13811" rel="nofollow">#13811</a>, and <a href="https://trac.torproject.org/projects/tor/ticket/12831" rel="nofollow">#12831</a>.</p>

<h3>Proposal 227: Include package fingerprints in consensus documents</h3>

<p>This one should help the TorBrowser updater: it allows directory authorities to vote on, and include in their consensus documents, a set of versions, URLs, and fingerprints for various software packages.  Now every Tor client will have an easy way to learn when a new package is available, and to authenticate the package digest.</p>

<p>More info: <a href="https://gitweb.torproject.org/torspec.git/tree/proposals/227-vote-on-package-fingerprints.txt" rel="nofollow">Proposal 227</a>, <a href="https://trac.torproject.org/projects/tor/ticket/10395" rel="nofollow">#10395</a>.</p>

<h3>More efficient directory requests</h3>

<p>Back when I wrote the code for compressing responses on directory requests, I didn't use all the right flags for zlib.  By turning off the "Z_FLUSH" flag on our compression objects, I think we should be saving something like 7% of the bandwidth currently used for compressed microdescriptors and descriptors, freeing that bandwidth up for other purposes.</p>

<p>More info: <a href="https://trac.torproject.org/projects/tor/ticket/11787" rel="nofollow">#11787</a>.</p>

<h3>Improved memory-based DoS prevention</h3>

<p>In 0.2.5, we introduced features to prevent accidental or deliberate out-of-memory conditions from taking down Tor nodes.  (The <a href="https://blog.torproject.org/blog/new-tor-denial-service-attacks-and-defensesf" rel="nofollow">"Sniper Attack"</a> research explains why this is really important.)</p>

<p>In 0.2.6, we identify more kinds of memory usage that could grow without bound, and improve our memory tracker to consider them and clear them as needed.</p>

<p>Also, we add a notion of a "low memory" mode.  When a Tor relay is low on memory, it tries to use less memory-heavy versions of its algorithms.  (Currently, this only effects zlib compression.)</p>

<p>More info: <a href="https://trac.torproject.org/projects/tor/ticket/13806" rel="nofollow">#13806</a>, <a href="https://trac.torproject.org/projects/tor/ticket/10115" rel="nofollow">#10115</a>, <a href="https://trac.torproject.org/projects/tor/ticket/11792" rel="nofollow">#11792</a>.</p>

<h3>Hidden service statistics support</h3>

<p>Tor relays can now collect statistics on the amount of traffic that is due to hidden service usage, and the total number of hidden services in existence.  (Yes, we have made a major effort to ensure this is done safely, by aggregating results, adding laplace noise, and doing lots and lots of analysis.)</p>

<p>This code is off by default; you can help us learn more about the network by enabling the HiddenServiceStatistics option.</p>

<p>More info: <a href="https://gitweb.torproject.org/torspec.git/tree/proposals/238-hs-relay-stats.txt" rel="nofollow">Proposal 238</a>, <a href="https://trac.torproject.org/projects/tor/ticket/13192" rel="nofollow">#13192</a>.</p>

<h3>Dropped support for various old things</h3>

<p><i>Panta rhei;</i> the world is ever-changing.  Once upon a time, it might have made sense for us to support the C89 C standard, versions of Windows before XP, kernels without threading support, and the WinCE (!) architecture.</p>

<p>Windows XP is a special case.  In spite of its lack of security updates, some folks are still on it, and I'm not going to deliberately drop support for it for a while longer.  Nevertheless, we can't keep the romance alive forever here: Sooner or later (probably) sooner, we'll wind up accidentally breaking WinXP.  When that happens, we're not likely to fix it: running an OS without security patches is the internet equivalent of rejecting vaccines.</p>

<p>More info: <a href="https://trac.torproject.org/projects/tor/ticket/11446" rel="nofollow">#11446</a>, <a href="https://trac.torproject.org/projects/tor/ticket/12439" rel="nofollow">#12439</a>, <a href="https://trac.torproject.org/projects/tor/ticket/13233" rel="nofollow">#13233</a>.</p>

<h3>Proposal 232: Pluggable Transport through SOCKS proxy</h3>

<p>When Tor is configured to use a pluggable transport, *and* configured to send data through an outgoing SOCKS proxy, it can now tell its transports about that proxy, too. This isn't a feature most people need, but the people who need it <em>really</em> need it. </p>

<p>More info: <a href="https://gitweb.torproject.org/torspec.git/tree/proposals/232-pluggable-transports-through-proxy.txt" rel="nofollow">Proposal 232</a>, <a href="https://trac.torproject.org/projects/tor/ticket/8402" rel="nofollow">#8402</a>.</p>

<h3>Longer guard-node lifetimes, single chosen guards</h3>

<p>This one is going to be important for security.  Earlier research indicates that we need to have each client use fewer guards for their traffic, and keep each guard around for longer, if we want to strengthen their resistance to attackers running a large number of nodes.  We've tweaked these values a bit already; current defaults are one entry guard, three guards for directory info, and sixty to ninety days for a guard lifetime.  But once we finally merge the <a href="https://trac.torproject.org/projects/tor/ticket/9321" rel="nofollow">#9321</a> patch, we can increase the guard lifetime to something much higher without unbalancing clients' bandwidth distributions.</p>

<p>More info: <a href="https://trac.torproject.org/projects/tor/ticket/9321" rel="nofollow">#9321</a> and its child tickets, "<a href="https://blog.torproject.org/blog/improving-tors-anonymity-changing-guard-parameters" rel="nofollow">Improving Tor's Anonymity by Changing Guard Parameters</a>", and "<a href="https://www.petsymposium.org/2014/papers/Dingledine.pdf" rel="nofollow">One Fast Guard For Life</a>".</p>

<h3>And miscellaneous features and bugfixes too numerous to mention...</h3>

<p>See the <a href="https://gitweb.torproject.org/tor.git/tree/ChangeLog" rel="nofollow">ChangeLog</a> for all the exciting details.  </p>

<h3>Additional acknowledgments:</h3>

<p>Thanks go to everybody who has helped with this release, either by writing code, reporting bugs, testing fixes, making comments, diagnosing issues, or just generally helping out.  This includes, but is not limited to:  Adrien BAK, Andrea Shepard, Anthony G. Basile, Arlo Breault, Arthur Edelstein, Craig Andrews, David Goulet, David Stainton, Francisco Blas Izquierdo Riera (klondike), George Kadianakis, Jens Kubieziel, Dana Koch, Isis Lovecruft, Jacob Appelbaum, Karsten Loesing, Kevin Murray, Magnus Nord, Mansour Moufid, Matthew Finkel, Michael Scherer, Peter Palfrader, Philip Van Hoof, Roger Dingledine, Sebastian Hahn, Mark Smith, Gisle Vanem, Tom van der Woerdt, Tomasz Torcz, Yawning Angel, anonymous, cypherpunks, intrigeri, meejah, rl1987, teor, skruffy, special, wfn, fpxnns, jowr, Device, and chobe.</p>

---
_comments:

<a id="comment-89219"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89219" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 03, 2015</p>
    </div>
    <a href="#comment-89219">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89219" class="permalink" rel="bookmark">I past it was said that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I past it was said that having a NumCPU value greater than 2 was pointless.</p>
<p>Is it now true that greater values are useful in v0.2.6.x?  If so, what is the practical limit now?  Will 4 cores be used effectively now for a single instance of Tor?  How about 8 cores?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-89228"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89228" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 04, 2015</p>
    </div>
    <a href="#comment-89228">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89228" class="permalink" rel="bookmark">Please Put New and useful</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please Put New and useful Bridges ! </p>
<p>using  Tor Each day is  being limited</p>
<p>it just works obf3 ,obf4 and meek- azure </p>
<p>Almost all bridges have been blocked<br />
it is not possible to connect to the Tor  (using Tails)in Iran</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-89647"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89647" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 11, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-89228" class="permalink" rel="bookmark">Please Put New and useful</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-89647">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89647" class="permalink" rel="bookmark">Bridges aren&#039;t put up by the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Bridges aren't put up by the Tor Project. Individual users setup bridge nodes and maintain them. If you have any family in the US or another pro-Tor country you could ask them to run a private bridge that only you could connect to. Private bridges aren't published to the directory and thus would be hard to find for anyone without the bridge's address.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-89757"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89757" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 12, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-89647" class="permalink" rel="bookmark">Bridges aren&#039;t put up by the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-89757">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89757" class="permalink" rel="bookmark">Is it really so hard to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is it really so hard to detect a bridge connected to published tor entry guard? Why are you so naive? Or when you say pro-tor you mean pro-nsa.<br />
Anyway if you have the trusted party in the other country you may just setup vpn. So it will be _impossible_ to connect to your entry point for anyone but you. (ssl)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-89230"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89230" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 04, 2015</p>
    </div>
    <a href="#comment-89230">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89230" class="permalink" rel="bookmark">Where can I share an idea of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Where can I share an idea of a great project that would be PERFECT for the tor community?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-89534"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89534" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 09, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-89230" class="permalink" rel="bookmark">Where can I share an idea of</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-89534">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89534" class="permalink" rel="bookmark">Try the tor-talk list or the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Try the tor-talk list or the #tor IRC channel on oftc.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-89320"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89320" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 05, 2015</p>
    </div>
    <a href="#comment-89320">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89320" class="permalink" rel="bookmark">can tor browser create on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>can tor browser create on google chrome...?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-89392"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89392" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 06, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-89320" class="permalink" rel="bookmark">can tor browser create on</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-89392">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89392" class="permalink" rel="bookmark">Please read:</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please read: <cite><a href="https://trac.torproject.org/projects/tor/wiki/doc/ImportantGoogleChromeBugs" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/doc/ImportantGoogleChrome…</a></cite></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-89443"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89443" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 07, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-89320" class="permalink" rel="bookmark">can tor browser create on</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-89443">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89443" class="permalink" rel="bookmark">No, not for the moment.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No, not for the moment. There are privacy issues.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-89388"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89388" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 06, 2015</p>
    </div>
    <a href="#comment-89388">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89388" class="permalink" rel="bookmark">problem from Iran again. I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>problem from Iran again. I can not connect to tor easily.<br />
have tried from diffrent networks, but the same result.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-89426"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89426" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 06, 2015</p>
    </div>
    <a href="#comment-89426">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89426" class="permalink" rel="bookmark">There are only several</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There are only several thousand circuits. It is the most serious problem.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-89628"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89628" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 10, 2015</p>
    </div>
    <a href="#comment-89628">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89628" class="permalink" rel="bookmark">All the obfs4 bridges found</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>All the obfs4 bridges found on bridgedb are unavailable in China.</p>
</div>
  </div>
</article>
<!-- Comment END -->
