title: CALEA 2 and Tor
---
pub_date: 2013-05-09
---
author: arma
---
tags:

law enforcement
data retention
advocacy
---
categories: advocacy
---
_html_body:

<p>Journalists and activists have been asking me this week about the news that the Obama administration is now <a href="http://www.nytimes.com/2013/05/08/us/politics/obama-may-back-fbi-plan-to-wiretap-web-users.html" rel="nofollow">considering</a> whether to support the latest version of the FBI's <a href="https://www.eff.org/foia/foia-records-problems-electronic-surveillance" rel="nofollow">"Going Dark"</a> legislation. Here are some points to add to the discussion.</p>

<ul>
<li>This is far from law currently. Nobody's even published any proposed text. Right now the White House is considering whether to back it, and now is a great time to help them understand how dangerous it would be for America.</li>
<li>Forcing backdoors in communication tools is a mandate for insecurity. Haven't they been paying attention to just how much these same systems are under <a href="http://googleblog.blogspot.com/2011/06/ensuring-your-information-is-safe.html" rel="nofollow">attack</a> from foreign governments and criminals? Did they not learn any lessons from the wiretapping scandals in <a href="http://en.wikipedia.org/wiki/Greek_wiretapping_case_2004%E2%80%932005" rel="nofollow">Greece</a> and <a href="http://en.wikipedia.org/wiki/SISMI-Telecom_scandal" rel="nofollow">Italy</a>, where CALEA backdoors were used to surveil politicians, without law enforcement even knowing about it? You cannot add a backdoor to a communications system without making it more vulnerable to attack, both from insiders and from the outside.</li>
<li>The Justice Department is being really short-sighted here by imagining that the world is black and white. We've heard from people at the <a href="https://blog.torproject.org/blog/trip-report-october-fbi-conference" rel="nofollow">FBI</a>, DEA, NSA, etc who <a href="https://www.torproject.org/about/torusers.html.en#lawenforcement" rel="nofollow">use Tor for their job</a>. If we changed the design so we could snoop on people, those users should go use a system that isn't broken by design — such as one in another country. And if those users should, why wouldn't criminals switch too?</li>
<li>In any case, it seems likely that the law won't apply to The Tor Project, since we don't run the Tor network and also it's not a service. (We write free open source software, and then people run it to form a network.)</li>
<li>The current <a href="http://en.wikipedia.org/wiki/Communications_Assistance_for_Law_Enforcement_Act" rel="nofollow">CALEA</a> already has an ugly <a href="https://blog.torproject.org/blog/trip-report-german-foreign-office" rel="nofollow">trickle-down effect</a> on the citizens of other countries. Different governments have different standards for lawful access, but the technology doesn't distinguish. So when the Egyptian general plugs in his telco box and sees the connector labelled "lawful access", he thinks to himself "I *am* the law" and proceeds with surveilling his citizens to stay in power. To put it bluntly, America's lawful intercept program undermines its foreign policy goals.</li>
</ul>

<p>And lastly, we should all keep in mind that they can't force us to do anything. You always have the alternative of stopping whatever it is you're doing. So for example if they try to "force" an individual <a href="https://www.torproject.org/docs/faq#KeyManagement" rel="nofollow">directory authority</a> operator to do something, the operator should just stop operating the authority (and then consider working with EFF and ACLU to establish precedent that such an attempt was illegal). And so on, all the way up the chain. Good thing the Internet is an international community.</p>

---
_comments:

<a id="comment-20684"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-20684" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">May 09, 2013</p>
    </div>
    <a href="#comment-20684">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-20684" class="permalink" rel="bookmark">For those who don&#039;t already</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>For those who don't already know, we also have a FAQ entry about Tor and backdoors:<br />
<a href="https://www.torproject.org/docs/faq#Backdoor" rel="nofollow">https://www.torproject.org/docs/faq#Backdoor</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-20745"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-20745" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 09, 2013</p>
    </div>
    <a href="#comment-20745">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-20745" class="permalink" rel="bookmark">Can you give some context as</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can you give some context as to what is going on here?<br />
I use Tor everyday to post online anonymously...what is Obama up to now?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-20746"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-20746" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">May 09, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-20745" class="permalink" rel="bookmark">Can you give some context as</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-20746">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-20746" class="permalink" rel="bookmark">That&#039;s why I linked to the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That's why I linked to the NYT article at the beginning:<br />
<a href="http://www.nytimes.com/2013/05/08/us/politics/obama-may-back-fbi-plan-to-wiretap-web-users.html" rel="nofollow">http://www.nytimes.com/2013/05/08/us/politics/obama-may-back-fbi-plan-t…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-20852"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-20852" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 10, 2013</p>
    </div>
    <a href="#comment-20852">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-20852" class="permalink" rel="bookmark">You are missing the point.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You are missing the point. The law is irrelevant. For will be made illegal, or just fined to death and dragged through court for a decade until there is no more for. This is not a technical problem to solve. This is about control and abuse of power.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-21064"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-21064" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 13, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-20852" class="permalink" rel="bookmark">You are missing the point.</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-21064">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-21064" class="permalink" rel="bookmark">The law is clearly relevant,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The law is clearly relevant, or there would be no attempts to outlaw it.</p>
<p>Right now, Tor has not been "fined to death" (it cannot be fined unless it is illegal) or "dragged through court for a decade" (because doing that to a legal organization is bad publicity; ask Apple about Samsung).</p>
<p>If things were as bad as you said, Tor wouldn't exist right now.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-21505"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-21505" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 19, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-20852" class="permalink" rel="bookmark">You are missing the point.</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-21505">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-21505" class="permalink" rel="bookmark">Apparently your keyboard</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Apparently your keyboard mixes up 'F' and 'T'?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-21022"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-21022" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 12, 2013</p>
    </div>
    <a href="#comment-21022">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-21022" class="permalink" rel="bookmark">If Ron Paul had won the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If Ron Paul had won the presidential election then maybe we wouldn't be having this problem.. But, since Americans have lost all understanding of the meaning and value of liberty, they have therefore voted themselves into slavery....</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-21045"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-21045" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 12, 2013</p>
    </div>
    <a href="#comment-21045">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-21045" class="permalink" rel="bookmark">If you are too dumb to live</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you are too dumb to live in a liberal democracy then you will live in a totalitarian police state.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-21159"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-21159" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 14, 2013</p>
    </div>
    <a href="#comment-21159">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-21159" class="permalink" rel="bookmark">&quot;...they can&#039;t force us to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"...they can't force us to do anything. You always have the alternative of stopping whatever it is you're doing. So for example if they try to "force" an individual directory authority operator to do something, the operator should just stop operating the authority..."</p>
<p>I think they can force people to do things, including continuing to operate while compromised and keeping it a secret.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-21176"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-21176" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">May 14, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-21159" class="permalink" rel="bookmark">&quot;...they can&#039;t force us to</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-21176">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-21176" class="permalink" rel="bookmark">Remind me not to have you</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Remind me not to have you run a directory authority, or make compliance decisions of any sort for us. You always have a choice!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-21204"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-21204" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 15, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-21204">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-21204" class="permalink" rel="bookmark">I&#039;m actually the kind of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm actually the kind of person who would strongly consider going to prison for disobeying the government. In the United States, librarians were forced to hand over checkout histories to the federal government, and keep quiet about it. A few of them leaked it, and were imprisoned for saying something.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-21310"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-21310" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 16, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-21310">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-21310" class="permalink" rel="bookmark">I think he&#039;s right. Isn&#039;t</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I think he's right. Isn't that what happened at Hushmail? And that was Canada.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-21889"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-21889" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">May 23, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-21310" class="permalink" rel="bookmark">I think he&#039;s right. Isn&#039;t</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-21889">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-21889" class="permalink" rel="bookmark">No, Hushmail chose to put a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No, Hushmail chose to put a backdoor into their system and continue operating in Canada. Then I guess they moved most of their budget into PR to convince people that Hushmail was great and safe. Standard tactics from for-profit companies -- I'm glad we don't have shareholders.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-22188"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-22188" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 26, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-22188">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-22188" class="permalink" rel="bookmark">&quot;Standard tactics from</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"Standard tactics from for-profit companies -- I'm glad we don't have shareholders."</p>
<p>But plenty of your funding sources are for-profit companies, are they not?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-22534"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-22534" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 30, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-22534">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-22534" class="permalink" rel="bookmark">Hushmail is still better</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hushmail is still better than Gmail though...... right?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-22622"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-22622" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 30, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-22534" class="permalink" rel="bookmark">Hushmail is still better</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-22622">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-22622" class="permalink" rel="bookmark">What about TorMail?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What about TorMail?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-23582"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-23582" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 10, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-22622" class="permalink" rel="bookmark">What about TorMail?</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-23582">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-23582" class="permalink" rel="bookmark">I, too, would like to see an</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I, too, would like to see an official statement from the Tor Project on Tormail.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-32898"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-32898" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 07, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-23582" class="permalink" rel="bookmark">I, too, would like to see an</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-32898">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-32898" class="permalink" rel="bookmark">Tormail has nothing to do</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tormail has nothing to do with Tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-23583"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-23583" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 10, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-22534" class="permalink" rel="bookmark">Hushmail is still better</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-23583">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-23583" class="permalink" rel="bookmark">I wouldn&#039;t be so sure of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I wouldn't be so sure of that.</p>
<p>What about the argument by using a service like Hushmail, one is effectively *announcing*, "I've got something to hide..."?</p>
<p>It could possibly be said that there's a certain "safety in numbers" in the likes of Gmail; getting lost in the crowd.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-26108"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-26108" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 23, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-23583" class="permalink" rel="bookmark">I wouldn&#039;t be so sure of</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-26108">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-26108" class="permalink" rel="bookmark">This is a process of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is a process of accumulating and valuing associations, not determining a single value for a data point that applies to everyone. The model is not "if the number of emails from everyone containing the word 'Snowden' is big enough, it moves from suspicious activity to harmless for everyone"; the value of a data point fluctuates based on the other data points connected to it.</p>
<p>Consider a database query cross referencing every Gmail identity with at least one message containing 'Wikileaks' with Google searches for Tor. Then do it against sets like signers of online petitions and contributors to EFF. Now repeat with data of offline activity like protest attendance, membership in the ACLU, travel to London and Madrid (which both had train bombings) and purchases of books by Cory Doctorow. Once a threshold is passed, the value of an email that includes "Snowden" from the same account increases because the entire history is scrutinized for information that supports further investigation. None of the individual data points is illegal or even suspicious on its own, but together they are used to build a character sketch that triggers further intrusion.</p>
<p>Cherry-picking data carries a substantial danger of confirmation bias. Like someone in a messy breakup can easily fall into the trap of recalling the entire relationship and recasting every innocent mistake and misspoken word to support a case that the partner was betraying him the whole time, the context assigns motivations after the fact. These are based on the subjective interpretation of the analyst once the breakup has taken place rather than the motivation of the subject when she performed the acts in question. Positive mitigating factors like shared experiences and values are ignored and forgotten because they do not contribute to the predetermined judgment.</p>
<p>Herd immunity relies on storage and searching being relatively time consuming and expensive processes. Since these are now incredibly fast and cheap (and the budget virtually unlimited), the limiting factor moves down the line to how the information is used. Without meaningful oversight and respect for democratic values, the momentum is toward greater scope and secrecy with less responsibility for the watchers and fewer rights and protections for the people.</p>
<p>Tl;dr: There is no safety because you can't get lost.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-23462"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-23462" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 09, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-23462">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-23462" class="permalink" rel="bookmark">Arma, could you please</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><b>Arma</b>, could you please clarify what you mean by <i>"backdoor"</i> in Hushmail?</p>
<p>I know that Hushmail complies with legal warrants, and I am perfectly ok with that. Have I missed something else? Please give references to articles.</p>
<p>I want also to thank you for all your work with Tor and helping to protect us netizens.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-21311"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-21311" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 16, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-21311">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-21311" class="permalink" rel="bookmark">Well, there&#039;s this from the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Well, there's this from the article:</p>
<p>"Instead, the new proposal focuses on strengthening wiretap orders issued by judges. Currently, such orders instruct recipients to provide technical assistance to law enforcement agencies, leaving wiggle room for companies to say they tried but could not make the technology work."</p>
<p>So judges are even now forcing companies to engage in affirmative actions, to assign staff to work on wiretapping attempts.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-21179"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-21179" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 14, 2013</p>
    </div>
    <a href="#comment-21179">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-21179" class="permalink" rel="bookmark">The AP should have been</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The AP should have been using TBB and Tails:)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-21476"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-21476" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 18, 2013</p>
    </div>
    <a href="#comment-21476">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-21476" class="permalink" rel="bookmark">Sounds pretty weak, almost</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sounds pretty weak, almost like you would comply if the law told you to put a backdoor in tor. Pretty sad, FOSS is supposed to be immune from this stuff. </p>
<p>This law or a law like it will eventually be passed. Every law they want to pass they pass.</p>
<p>You should disobey the law. If they come for you, do worse to them than what they would do to you.</p>
<p>The "we don't think it is a good idea" I'm hearing from the FOSS community sounds like something out of soviet russia. Very weak.</p>
<p>Should be "fsk you, we will not capitulate, ever, even if that means a shooting war and our deaths"</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-21890"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-21890" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">May 23, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-21476" class="permalink" rel="bookmark">Sounds pretty weak, almost</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-21890">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-21890" class="permalink" rel="bookmark">Disobey what law? There</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Disobey what law? There isn't even any proposed law yet.</p>
<p>I tried for a while to work in a reference to <a href="https://www.torproject.org/docs/faq#Backdoor" rel="nofollow">https://www.torproject.org/docs/faq#Backdoor</a></p>
<p>But in the end I decided that this wasn't the right point for aggressively picking a fight. IMO the feds would be mighty foolish to pick a fight with Tor, first because we are the extreme example of why their upcoming law doesn't take reality into account, and (related) because we have so many friends around the world who would get upset alongside us, and help make sure the attempt backfires.</p>
<p>If they really want to make this our fight, we'll oblige them. But we've got a lot of other fights to fight, so I am not too eager to get too distracted from the rest of them.</p>
<p>(Historically, we're a "write code to make the world better" company, not a "stand around in a courtroom explaining how we want the world to be" company. I think we do better at the former. Also there are plenty of organizations who can do the latter.)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-21478"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-21478" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 18, 2013</p>
    </div>
    <a href="#comment-21478">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-21478" class="permalink" rel="bookmark">&quot;If we changed the design so</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"If we changed the design so we could snoop on people,"</p>
<p>Why are you even contemplating obeying them?<br />
The law is not your religion, it is the dictates of some overbearing enemy.<br />
Why would you obey them?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-22817"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-22817" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 01, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-21478" class="permalink" rel="bookmark">&quot;If we changed the design so</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-22817">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-22817" class="permalink" rel="bookmark">A conditional statement does</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A conditional statement does not imply what people intend to do.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-22407"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-22407" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 28, 2013</p>
    </div>
    <a href="#comment-22407">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-22407" class="permalink" rel="bookmark">With the latest scandals in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>With the latest scandals in the USA Government vs. The People, we can easily understand their motives with CALEA 2 and so on. Absolutely no more authorities for the Government criminals! Not that they matter anyway really...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-22532"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-22532" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 30, 2013</p>
    </div>
    <a href="#comment-22532">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-22532" class="permalink" rel="bookmark">I hope you guys will at</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I hope you guys will at least have the decency to tell us Tor users, once you are forced by the government to put a backdoor in to Tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-23584"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-23584" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 10, 2013</p>
    </div>
    <a href="#comment-23584">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-23584" class="permalink" rel="bookmark">First they came for the Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>First they came for the Tor users, and I didn't speak-out because I wasn't a Tor user, and I figured, "If you've got nothing to hide...."....</p>
</div>
  </div>
</article>
<!-- Comment END -->
