title: Vidalia 0.2.19 is out!
---
pub_date: 2012-05-14
---
author: chiiph
---
tags:

vidalia
stable
release
---
categories:

applications
releases
---
_html_body:

<p>Hello everybody,</p>

<p>I'm glad to announce that Vidalia stable has new releases, 0.2.18 and 0.2.19.<br />
They are not really big releases, but there are some fixes here to make it more comfortable for you while we get the alpha ready to be called stable.</p>

<p>Why two different releases? Well, errors occur and version numbers don't cost any money, so basically 0.2.18 was released, a couple of issues were found and then we made 0.2.19 fixing them.</p>

<p>You can find the source tarballs in here: <a href="https://www.torproject.org/dist/vidalia/" rel="nofollow">https://www.torproject.org/dist/vidalia/</a></p>

<p><b>IMPORTANT NOTE:</b> Vidalia releases have been in sync with Tor Browser Bundle releases. This won't necessarily happen anymore. It became hard to sync all the software that TBB contains, so Vidalia releases got delayed for more urgent Firefox issues and things like that. We therefor decided that Vidalia will be released when ready, and TBB will get it at some point.</p>

<p>So, if you don't want to build Vidalia from source, you need to be patient.</p>

<p>Here's what changed for 0.2.18 and 0.2.19:</p>

<p>0.2.18  14-May-2012</p>

<ul>
<li>Use consensus bandwidth for routers when microdescriptors is<br />
    enabled. Fixes bug 3287.
  </li>
<li>Notify users that a warning status event has appeared by flashing<br />
    the Message Log button. Fixes bug 3957.</li>
<li>Fix a method that didn't return the specified type if another<br />
    control method was selected. Fixes bug 4065.</li>
<li>Resume listing relays in the Network Map panel even when Tor only<br />
    offers microdescriptors (new in Tor 0.2.3.x). Fixes ticket 4203.</li>
<li>Handle unrecognized Tor bootstrap phases. (Tor tells us a summary<br />
    description of each bootstrap phase as it occurs, so we can use<br />
    that string if needed.) Fixes bug 5109.</li>
<li>Displays Korean, Catalan and Greek in their native scripts. Fix<br />
    bug 5110.</li>
<li>Support adding comments in the same line as a config option inside<br />
    the torrc dialog. Fixes bug 5475.</li>
<li>Remove Polipo and Torbutton from Vidalia's build scripts. Resolves<br />
    ticket 5769.</li>
<li>Fix deadlock when the browser process failed to launch in OS X.</li>
<li>Add ProfileDirectory, DefaultProfileDirectory, PluginsDirectory,<br />
    DefaultPluginsDirectory for better Browser configuration. Also set<br />
    the Vidalia binary location as the starting point for relative<br />
    paths.</li>
<li>Enable Burmese, Croatian, Basque and Serbian translation.</li>
<li>Remove the "Find bridges" button in order to avoid compromising<br />
    users that need to hide behind tor at all times. Fixes bug 5371.</li>
<li>Add visual feedback from VClickLabel when in "pressed" state.<br />
    Resolves ticket 5766.</li>
</ul>

<p>0.2.19  30-May-2012</p>

<ul>
<li>Disable "Run Vidalia when my system starts" if the<br />
    BrowserExecutable config option is set. This will avoid issues with<br />
    TBB users starting Vidalia the wrong way.</li>
<li>Automigrate TorExecutable, Torrc and DataDirectory config options<br />
    to the new relative path handling.</li>
<li>Really get rid of the openssl dependency. The goal had been to<br />
    achieve that for 0.2.18, but not everything was actually removed.</li>
<li>For static builds on windows, correctly link with zlib and<br />
    ws32_2.lib.</li>
</ul>

---
_comments:

<a id="comment-16120"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16120" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 13, 2012</p>
    </div>
    <a href="#comment-16120">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16120" class="permalink" rel="bookmark">Thank you for the new</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for the new Vidalia release. Why isn't there an unstable Vidalia Bundle (like Vidalia Relay Bundle etc.)?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-16126"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16126" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 13, 2012</p>
    </div>
    <a href="#comment-16126">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16126" class="permalink" rel="bookmark">Umm, you guys posted this</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Umm, you guys posted this under 14 May, but I think you meant 14 June.</p>
<p>This post is now appearing out of sequence in your blog entry list.</p>
</div>
  </div>
</article>
<!-- Comment END -->
