title: Tor Summer of Privacy--Apply Now!
---
pub_date: 2015-04-05
---
author: ailanthus
---
tags:

Google Summer of Code
students
Tor Summer of Privacy
Summer of Privacy
internships
---
categories: internships
---
_html_body:

<p>The Tor Project is launching our first Tor Summer of Privacy! This is a pilot program for students who want to collaborate to develop privacy tools. We participated in Google's groundbreaking Summer of Code from 2007-2014, but we weren't renewed this year (Google is rightly offering new groups this opportunity) so we've decided to start our own program. Many thanks to Tor's individual donors who decided to sponsor the Summer of Privacy. Students only have 10 days to apply--so spread the word!</p>

<p>We feel that working on Tor is rewarding because: </p>

<p>•    You will work with a world-class team of developers on an anonymity network that is already protecting millions of people daily--or work on your own, new project. </p>

<p>•    We only write free (open source) software. The tools you make won't be locked down or rot on a shelf. </p>

<p>•    The work you do could contribute to academic publications — Tor development raises many open questions and interesting problems in the field of anonymity systems <a href="http://freehaven.net/anonbib/" rel="nofollow">http://freehaven.net/anonbib/</a>. </p>

<p>•    You can work your own hours wherever you like. </p>

<p>•    We are friendly and collaborative. </p>

<p>We are looking for people with great code samples who are self-motivated and able to work independently. We have a thriving and diverse community of interested developers on the IRC channel and mailing lists, and we're eager to work with you, brainstorm about design, and so on, but you need to be able to manage your own time, and you need to already be somewhat familiar with how free software development on the Internet works. </p>

<p>We invite and welcome applications from many different kinds of students who come from many different backgrounds. Don't be shy--apply! </p>

<p>Tor will provide a total stipend of USD $5,500 per accepted student developer.</p>

<p><b>DEADLINE FOR APPLICATION: We are accepting applications now through April 17th, 2015. <a href="https://trac.torproject.org/projects/tor/wiki/org/TorSoP" rel="nofollow">Apply soon!</a></b></p>

<p>We're always happy to have new contributors, so if you are still planning your summer, please consider spending some time working with us to make Tor better!</p>

---
_comments:

<a id="comment-91973"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-91973" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 06, 2015</p>
    </div>
    <a href="#comment-91973">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-91973" class="permalink" rel="bookmark">I really am looking forward</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I really am looking forward to contributing to Tor. By that, I mean helping to make Tor. I'm not a coder but I'd like to help. I only know basic C. What thing do I have to learn to so that I can help make Tor? The core code of Tor is especially interesting :) I have K&amp;R 2nd E. and Hacking: The Art of Exploitation 2nd e. which looks like good books to start learning. Thoughts? Thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-91986"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-91986" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 06, 2015</p>
    </div>
    <a href="#comment-91986">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-91986" class="permalink" rel="bookmark">I wish all the best to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I wish all the best to everyone who will participate in the Summer of Privacy, and I hope applicants are so eager to get started that they will be willing to spend some time before summer arrives in a course of background reading.  May I recommend two Great Books which I believe will help you think like an attacker?</p>
<p>David Kahn<br />
The Codebreakers<br />
MacMillan, 1967</p>
<p>Bruce Schneier<br />
Applied Cryptography<br />
Wiley, 1996</p>
<p>How often does it happen that an "unauthorized history" of some arcane field inspires an epochal contribution to that very same field?  One of the few examples known to me: Kahn's book directly inspired Whitfield Diffie's work on what is now known as public-key cryptography.</p>
<p>One of the Snowden leaks so far published by The Intercept is the official NSA account of how NSA raided the home of W. F. Friedman, the legendary cryptographer for whom the Friedman auditorium at NSA/Washington is named.</p>
<p>Working on behalf of the USG in the years before World War Two, WFF had masterminded MAGIC, and after that war he briefly worked for the then new agency, NSA, but by the time of the raid on his home he had become an (internally) outspoken critic, on the grounds that NSA was continuing to carry on widespread violations of privacy of ordinary people around the world, which WFF felt was inappropriate in peace time.  Were he alive today, I have no doubt that WFF would be writing editorials supporting the views of William Binney, not those of Admiral Rogers.</p>
<p>The NSA agents who raided Friedman's home were looking for unclassified papers which Friedman had published prior to World War One while working at Riverbank Laboratories for an eccentric millionaire on the authorship of the plays of the author generally known as "Shakespeare".  (Who may well have been the historical actor/producer William Shakespeare, putting aside that controversy, I should point out that the analogies between the Elizabethan secret police, Stasi, KGB, NSA, and the creative responses of the targeted intelligentsia, are extensive and thought-provoking.)  The leaked NSA memo notes that during the raid, Friedman quietly smoked a pipe and appeared oddly unperturbed.  The reason was that WF had previously given a complete set of the legendary Riverbank papers to a bright neighbor boy, an aspiring reporter named, you guessed it, David Kahn!</p>
<p>Friedman met his wife Elizebeth at Riverbank.  She was herself an accomplished cryptanalyst who worked for the USCG decrypting the communications of rumrunners during the Prohibition era (as did WFF, briefly).  The similarities with the war of the modern FBI on the so-called Darknet are hard to miss, and instructive.</p>
<p>Bruce Schneier is not only a skilled cryptanalyst but also one of the world's most accomplished nonfiction authors.  All his books are well worth reading, but Applied Cryptography is the one every cryptanalyst studies.</p>
<p>There is a huge unmet need for a third great book, on modern cryptanalysis, which is not the same thing as techniques for black hat hacking, which is SOP for NSA nowadays.  (You can learn all about cryptanalysis up to Friedman's Riverbank papers from Kahn, and contrary to what some modern cryptanalysts state, I think those methods remain relevant today.)  The best standin, I think, are the blogs of another skilled cryptanalyst who is also a gifted writer, Matthew Green.   Since his blog is in constant danger of being censored/removed by our enemies at FBI/NSA, grab these while you can:</p>
<p><a href="http://blog.cryptographyengineering.com/2011/11/in-defense-of-applied-cryptography.html" rel="nofollow">http://blog.cryptographyengineering.com/2011/11/in-defense-of-applied-c…</a><br />
In defense of Applied Cryptography<br />
Matthew Green<br />
7 Nov 2011</p>
<p><a href="http://blog.cryptographyengineering.com/2011/09/brief-diversion-beast-attack-on-tlsssl.html" rel="nofollow">http://blog.cryptographyengineering.com/2011/09/brief-diversion-beast-a…</a><br />
A diversion: BEAST Attack on TLS/SSL Encryption<br />
Matthew Green<br />
20 Sep 2011</p>
<p><a href="http://blog.cryptographyengineering.com/2011/11/non-governmental-crypto-attacks.html" rel="nofollow">http://blog.cryptographyengineering.com/2011/11/non-governmental-crypto…</a><br />
Non-governmental crypto attacks<br />
Mattthew Green<br />
28 Nov 2011</p>
<p><a href="http://blog.cryptographyengineering.com/2012/01/openssl-and-nss-are-fips-140-certified.html" rel="nofollow">http://blog.cryptographyengineering.com/2012/01/openssl-and-nss-are-fip…</a><br />
OpenSSL and NSS are FIPS 140 certified. Is the Internet safe now?<br />
Matthew Green<br />
2 Jan 2012</p>
<p><a href="http://www.h-online.com/security/news/item/Trustwave-issued-a-man-in-the-middle-certificate-1429982.html" rel="nofollow">http://www.h-online.com/security/news/item/Trustwave-issued-a-man-in-th…</a><br />
Trustwave issued a man-in-the-middle certificate<br />
Matthew Green<br />
7 Feb 2012</p>
<p><a href="http://blog.cryptographyengineering.com/2013/03/attack-of-week-rc4-is-kind-of-broken-in.html" rel="nofollow">http://blog.cryptographyengineering.com/2013/03/attack-of-week-rc4-is-k…</a><br />
Attack of the week: RC4 is kind of broken in TLS<br />
Matthew Green<br />
12 Mar 2013</p>
<p><a href="http://blog.cryptographyengineering.com/2013/04/wonkery-mailbag-ideal-ciphers.html" rel="nofollow">http://blog.cryptographyengineering.com/2013/04/wonkery-mailbag-ideal-c…</a><br />
The Ideal Cipher Model (wonky)<br />
Matthew Green<br />
11 Apr 2013</p>
<p><a href="http://blog.cryptographyengineering.com/2013/05/a-few-thoughts-on-cellular-encryption.html" rel="nofollow">http://blog.cryptographyengineering.com/2013/05/a-few-thoughts-on-cellu…</a><br />
On cellular encryption<br />
Matthew Green<br />
14 May 2013</p>
<p><a href="http://blog.cryptographyengineering.com/2013/06/how-to-backdoor-encryption-app.html" rel="nofollow">http://blog.cryptographyengineering.com/2013/06/how-to-backdoor-encrypt…</a><br />
How to 'backdoor' an encryption app<br />
Matthew Green<br />
17 Jun 2013</p>
<p><a href="http://blog.cryptographyengineering.com/2013/09/on-nsa.html" rel="nofollow">http://blog.cryptographyengineering.com/2013/09/on-nsa.html</a><br />
On the NSA<br />
Matthew Green<br />
6 Sep 2013</p>
<p><a href="http://blog.cryptographyengineering.com/2013/09/the-many-flaws-of-dualecdrbg.html" rel="nofollow">http://blog.cryptographyengineering.com/2013/09/the-many-flaws-of-duale…</a><br />
The Many Flaws of Dual_EC_DRBG<br />
Mathew Green<br />
18 Sep 2013</p>
<p><a href="http://blog.cryptographyengineering.com/2013/12/how-does-nsa-break-ssl.html" rel="nofollow">http://blog.cryptographyengineering.com/2013/12/how-does-nsa-break-ssl…</a><br />
How does the NSA break SSL?<br />
Matthew Green<br />
2 Dec 2013</p>
<p><a href="http://blog.cryptographyengineering.com/2014/03/how-do-you-know-if-rng-is-working.html" rel="nofollow">http://blog.cryptographyengineering.com/2014/03/how-do-you-know-if-rng-…</a><br />
How do you know if an RNG is working?<br />
Matthew Green<br />
19 Mar 2014</p>
<p><a href="http://blog.cryptographyengineering.com/2014/04/attack-of-week-openssl-heartbleed.html" rel="nofollow">http://blog.cryptographyengineering.com/2014/04/attack-of-week-openssl-…</a><br />
Attack of the week: OpenSSL Heartbleed<br />
Matthew Green<br />
8 Apr 2014</p>
<p><a href="http://blog.cryptographyengineering.com/2014/04/attack-of-week-triple-handshakes-3shake.html" rel="nofollow">http://blog.cryptographyengineering.com/2014/04/attack-of-week-triple-h…</a><br />
Attack of the Week: Triple Handshakes (3Shake)<br />
Matthew Green<br />
24 Apr 2014</p>
<p><a href="http://blog.cryptographyengineering.com/2014_07_01_archive.html" rel="nofollow">http://blog.cryptographyengineering.com/2014_07_01_archive.html</a><br />
Noodling about IM protocols<br />
Matthew Green<br />
26 July 2014</p>
<p><a href="http://blog.cryptographyengineering.com/2014/10/attack-of-week-poodle.html" rel="nofollow">http://blog.cryptographyengineering.com/2014/10/attack-of-week-poodle.h…</a><br />
Attack of the week: POODLE<br />
Matthew Green<br />
14 Oct 2014</p>
<p><a href="http://blog.cryptographyengineering.com/2015/03/attack-of-week-freak-or-factoring-nsa.html" rel="nofollow">http://blog.cryptographyengineering.com/2015/03/attack-of-week-freak-or…</a><br />
Attack of the week: FREAK (or 'factoring the NSA for fun and profit')<br />
Matthew Green<br />
3 Mar 2015</p>
<p>Please note that many of these blog posts offer detailed studies of practical attacks on TLS, which should help to explain why I hope Tor coders will read them, if they have not already done so.  If one had to pick just a few of Green's posts, I'd pick the one on RNGs (both Snowden and Green have stated that attacking the RNG is much easier for NSA than full-on cryptanalysis of a modern block cipher).</p>
<p>To repeat, I am suggesting that students who have applied to the Summer of Tor read as much of these as practical over the next few months, not that anyone try to read them all in a day.  One day-planning warning: once you pick up Kahn's rather long book, I can almost guarantee that you will be unwilling to stop reading until you've finished the book, and then you'll want to immediately read it all over again. It's that good.</p>
<p>Enjoy!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-91991"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-91991" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 06, 2015</p>
    </div>
    <a href="#comment-91991">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-91991" class="permalink" rel="bookmark">Those people who apply to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Those people who apply to this will be monitored, tracked, or harrased by NSA and GCHQ.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-92004"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-92004" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">April 07, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-91991" class="permalink" rel="bookmark">Those people who apply to</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-92004">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-92004" class="permalink" rel="bookmark">Well, that isn&#039;t a very nice</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Well, that isn't a very nice thing to say. I think it's also a wrong thing to say.</p>
<p>First, why would they be any more interesting than the millions of other people who care about Tor?</p>
<p>And second, giving people the impression that if they do X then they'll be scooped up in the pervasive monitoring, but if they don't do X then they won't be, is dangerously misleading. The pervasive monitoring by many of these groups scoops up everything, whether you do X or not.</p>
<p>In short, be careful with logic that includes "I won't do this activity and then they won't watch me".</p>
<p><a href="https://blog.torproject.org/blog/being-targeted-nsa#comment-64219" rel="nofollow">https://blog.torproject.org/blog/being-targeted-nsa#comment-64219</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-92000"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-92000" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 07, 2015</p>
    </div>
    <a href="#comment-92000">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-92000" class="permalink" rel="bookmark">TBB 4.5a5 is very useful for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>TBB 4.5a5 is very useful for screen size faking when you choose privacy and security settings from the green onion icon, and set the security level to medium low or above. when you adjust tbb windows size to an unnormal size, it helps it stays the same or adjust another type of standard screen revolution.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-92068"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-92068" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 08, 2015</p>
    </div>
    <a href="#comment-92068">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-92068" class="permalink" rel="bookmark">Someone suggested:
&gt; Those</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Someone suggested:</p>
<p>&gt; Those people who apply to [Summer of Tor] will be monitored, tracked, or harassed by NSA and GCHQ.</p>
<p>Well, they're tracking absolutely everyone, but no doubt Summer of Tor coders will become particular targets.  But so what?  We can't let fear of becoming a "selectee" deter us from combating state-sponsored criminality.</p>
<p>Secret police states create an atmosphere of fear in order to deter their restive populations from effective opposition to oppressive governmental policies.  Freedom-loving citizens can, should, and will resist such intimidation by all means at their disposal.</p>
<p>Arma asked:</p>
<p>&gt; why would they be any more interesting than the millions of other people who care about Tor?</p>
<p>Because student developers </p>
<p>o may gain inside access to TP networks and employees,</p>
<p>o can possibly be more easily pressured by USG thugs than people with wider experience and more influential personal connections,</p>
<p>o may use hardware devices which can more easily be trojaned by NSA and its sidekicks in UK, CA, AU, NZ,</p>
<p>o may acquire knowledge of the personal strengths and weaknesses of key Tor Project members,</p>
<p>o may write code which is eventually used by millions, code which we can expect NSA will try to "shape" by any means possible, even if it cannot "turn" any of the student developers.</p>
<p>I am not saying that these possibilities-- or better say, "likelihoods"--- should dissuade anyone from fostering the growth of the privacy industry by sponsoring programs such as Summer of Tor!  Rather, I am warning everyone to remain vigilant.  Because, as arma said, just because we are not doing anything wrong does not mean FVEY agencies are not out to get us.  They are, and they routinely use vicious tactics.  So be careful.</p>
<p>For extensive details on how FVEY agencies try to "shape" anonymity and privacy-enhancing software and to disrupt the organizations which develop such software, please see Snowden-leaked documents on </p>
<p>o NSA presentations on Project Bullrun</p>
<p>o NSA/GCHQ presentations about several anti-Tor programs</p>
<p>o GCHQ presentations on disrupting targeted communities</p>
<p><a href="https://www.eff.org/nsa-spying/nsadocs" rel="nofollow">https://www.eff.org/nsa-spying/nsadocs</a></p>
<p>Know your enemy!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-92107"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-92107" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 09, 2015</p>
    </div>
    <a href="#comment-92107">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-92107" class="permalink" rel="bookmark">Just as a side note: Please</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Just as a side note: Please mark a blog post like this, that is pinned at the beginning of the blog. I needed some days to figure out that this post here isn't the latest one, but there has been some new ones published in between.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-92148"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-92148" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 10, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-92107" class="permalink" rel="bookmark">Just as a side note: Please</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-92148">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-92148" class="permalink" rel="bookmark">Same here! 
It took me at</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Same here! </p>
<p>It took me at least a day before I realized. I regularly check this blog for potentially critical information or just to see progress. It's true that I could have been more thorough in checking, but when you sometimes check several times a day a little added convenience is welcomed. </p>
<p>Tor blog Firefox add-on anyone? Something small (in our non-Tbb browser) that blinks/informs when a new post is created. It could even allow for someone to follow a particular post and it's comments. I just think keeping the community informed about important data/events is of utmost importance. With automatic updates reaching stability, some will feel less inclined to visit the blog. A plugin could bridge the upcoming gap.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-92232"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-92232" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 13, 2015</p>
    </div>
    <a href="#comment-92232">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-92232" class="permalink" rel="bookmark">Argh, I&#039;d love to apply, but</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Argh, I'd love to apply, but I'm graduating this May.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-92455"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-92455" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 23, 2015</p>
    </div>
    <a href="#comment-92455">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-92455" class="permalink" rel="bookmark">&gt; Argh, I&#039;d love to apply,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; Argh, I'd love to apply, but I'm graduating this May.</p>
<p>Employment should not be a binary choice between:</p>
<p>o Tor Project<br />
o the surveillance state</p>
<p>Plainly, Tor Project cannot possibly hope to hire the thousands of people who want to make a difference by working for a privacy-enhancing company.</p>
<p>Can recent graduates in such relevant fields as CS "create their own (privacy-enhancing) jobs" by helping to grow a privacy industry?  Which rigorously repels infiltration/subversion by the enemy?</p>
<p>Ideas?</p>
</div>
  </div>
</article>
<!-- Comment END -->
