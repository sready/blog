title: Online Anonymity Debate in South Korea
---
pub_date: 2008-10-17
---
author: phobos
---
tags:

online anonymity
south korea
cyber defamation
whistleblowers
democracy
---
_html_body:

<p><a href="http://www.tmcnet.com/usubmit/-online-anonymity-hotly-debated-south-korea-/2008/10/15/3707066.htm" rel="nofollow">An article</a> about the debate over online anonymity in South Korea caught my eye for a few reasons.  The topic of online anonymity periodically rises to the social consciousness in South Korea.  This time, it's about the suicide of a well-known actress, <a href="http://en.wikipedia.org/wiki/Choi_Jin-sil" rel="nofollow">Choi Jin-sil</a>.  It's sad that she chose to commit suicide, and I'm sorry she felt she had no where to go for help.  However, blaming the Internet for her death is dubious at best.  The Internet is a collection of networks.  The Internet is a thing, not a person.  While I dislike rude telemarketers, I don't blame the telephone company for providing the connection.  In this case, there seemed to be a subset of people bent on defaming her regardless of the circumstances, while using the Internet as their communication medium.  </p>

<p>The real goal behind this upswelling of support for banning anonymity is to pass the Cyber Defamation Law.  This appears to be the equivalent of user verification where all online activities must be in a real name and, in some way, verifiable.</p>

<blockquote><p>"We will press hard to pass the Cyber Defamation Law and the real-name system," Hong Joon-pyo, the ruling Grand National Party's floor leader, told reporters last week. "It is wrong to neglect the fact that violence is rampant online, due to anonymity."</p></blockquote>

<p>Defamation is already a crime in South Korea.  The irony is that the person who possibly first defamed Choi Jin-sil has been found, a person only known by their last name, Paik.  According to the article,<br />
</p>

<blockquote>"Paik was questioned by investigators soon after Choi's suicide and ultimately indicted for defamation."</blockquote>

<p>Normally, the articles on how online anonymity is bad for the world end here.  In this case, the article closes with how online anonymity is what probably helped bring South Korea into the democracy it has today.  </p>

<p>A great quote from Park Jun-chul,<br />
</p>

<blockquote>"If there is no anonymity, not so many people will risk saying what is really happening at work places, schools, or in the society."</blockquote>

<p>This ties right into what we've already seen happen online with activists,  <a href="https://www.torproject.org/torusers.html.en#activists" rel="nofollow">https://www.torproject.org/torusers.html.en#activists</a></p>

<p><a href="http://www.guardian.co.uk/technology/2008/oct/09/news.internet" rel="nofollow">The Guardian</a> has a better overview covering both the positive and negative uses of online anonymity in South Korea.</p>

