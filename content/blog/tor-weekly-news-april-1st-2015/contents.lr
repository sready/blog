title: Tor Weekly News — April 1st, 2015
---
pub_date: 2015-04-01
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the thirteenth issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Tor Browser 4.0.6 and 4.5a5 are out</h1>

<p>Mike Perry announced two new releases by the Tor Browser team. <a href="https://blog.torproject.org/blog/tor-browser-406-released" rel="nofollow">Tor Browser 4.0.6</a> contains updates to Firefox, meek, and OpenSSL; it is also the last release planned to run on 32-bit Apple hardware. If you have a 64-bit Mac and are running Mac OS X 10.8, you can expect to be automatically upgraded to Tor Browser 4.5, optimized for your hardware, later this month. If you are running OS X 10.6 or 10.7, however, you will need to update manually once that version of Tor Browser is released, as described in the <a href="https://blog.torproject.org/blog/tor-browser-45a5-released" rel="nofollow">end-of-life announcement last year</a>.</p>

<p><a href="https://blog.torproject.org/blog/end-life-plan-tor-browser-32-bit-macs" rel="nofollow">Tor Browser 4.5a5</a>, meanwhile, includes several exciting security and usability updates. Tor Browser’s windows, when resized, will now “snap” to one of a limited range of sizes, to prevent an adversary from fingerprinting a user based on their unique browser size; the Security Slider now offers information about the features that are disabled at each security level; and Tor circuits remain in use for a longer period, avoiding the errors that can result when websites detect a change in your connection. You can read about all these features and more in Mike’s announcement.</p>

<p>These new releases contain important security updates, and all users should upgrade as soon as possible. As usual, you can get your copy of the new software using the in-browser updater, or from the <a href="https://www.torproject.org/projects/torbrowser.html" rel="nofollow">project page</a>.</p>

<h1>Tails 1.3.2 is out</h1>

<p>Tails <a href="https://tails.boum.org/news/version_1.3.2" rel="nofollow">version 1.3.2</a> was put out on March 31. This release includes updates to key software, fixing numerous security issues. All Tails users must upgrade as soon as possible; see the announcement for download instructions.</p>

<h1>Crowdsourcing the future (of onion services)</h1>

<p>Onion (or hidden) services are web (or other) services hosted in the Tor network that have anonymity, authentication, and confidentiality built in. As George Kadianakis writes, “anything you can build on the Internet, you can build on hidden services — but they’re better”. A major task for the Tor community in the near future is making these important tools more widely available, and usable by groups who urgently need them, so George took to the <a href="https://blog.torproject.org/blog/crowdfunding-future-hidden-services" rel="nofollow">Tor blog</a> to solicit ideas for future onion service-related projects that could form the basis for a crowdfunding campaign. “Long story short, we are looking for feedback! What hidden services projects would you like to see us crowdfund? How do you use hidden services; what makes them important to you? How you want to see them evolve?…Also, we are curious about which crowdfunding platforms you prefer and why.”</p>

<p>See the full post for an introduction to onion services, why they matter, why a crowdfunding campaign makes sense, and how to join in with your own ideas.</p>

<h1>Spreading the word about Tor with free brochures</h1>

<p>Tor advocates play an important role in talking to groups and audiences around the world about the ways Tor and online anonymity can benefit them. Until now, printed materials offering a simple introduction to the basic concepts behind Tor have been hard to come by, so Karsten Loesing <a href="https://blog.torproject.org/blog/spread-word-about-tor" rel="nofollow">announced</a> a set of brochures, aimed at various audiences, that can be freely printed and distributed at Tor talks, tech conferences, public demonstrations, or just for fun. These will continue to receive updates and translations, so stay tuned.</p>

<p>If you don’t have access to printing facilities, you can contact the <a href="https://www.torproject.org/about/contact" rel="nofollow">Tor Project</a> with details of your event and requirements and receive a stack of brochures, possibly in return for a report or other feedback. Spread the word, and feel free to screen the <a href="https://blog.torproject.org/blog/releasing-tor-animation" rel="nofollow">Tor animation</a> in your language while you’re at it!</p>

<h1>Monthly status reports for March 2015</h1>

<p>The wave of regular monthly reports from Tor project members for the month of March has begun. <a href="https://lists.torproject.org/pipermail/tor-reports/2015-March/000782.html" rel="nofollow">Damian Johnson</a> released his report first, followed by reports from <a href="https://lists.torproject.org/pipermail/tor-reports/2015-March/000783.html" rel="nofollow">Tom Ritter</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2015-March/000784.html" rel="nofollow">Philipp Winter</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2015-March/000785.html" rel="nofollow">Pearl Crescent</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2015-April/000786.html" rel="nofollow">Nick Mathewson</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2015-April/000787.html" rel="nofollow">Juha Nurmi</a>, and <a href="https://lists.torproject.org/pipermail/tor-reports/2015-April/000788.html" rel="nofollow">Isabela Bagueros</a>.</p>

<h1>Miscellaneous news</h1>

<p>Anthony G. Basile <a href="https://lists.torproject.org/pipermail/tor-talk/2015-March/037355.html" rel="nofollow">announced</a> version 20150322 of tor-ramdisk, the micro Linux distribution whose only purpose is to host a Tor server in an environment that maximizes security and privacy. This release includes updates to Tor, busybox, OpenSSL, and the Linux kernel.</p>

<p>George Kadianakis used some <a href="https://lists.torproject.org/pipermail/tor-dev/2015-March/008541.html" rel="nofollow">newly-discovered bridge statistics</a> to generate visual bandwidth histories, in order “to better understand how much bridges are used”. “Questions and feedback on my methodology are welcome”, writes George. On the other hand, “we should think about the privacy implications of these statistics since they are quite fine-grained (multiple measurements per day) and some bridges don’t have many clients (hence small anonymity set for them)”, so if you have comments on this topic feel free to send them to the thread.</p>

<h1>News from Tor StackExchange</h1>

<p>Tor’s StackExchange site is currently running a <a href="http://meta.tor.stackexchange.com/questions/249/lets-get-critical-mar-2015-site-self-evaluation" rel="nofollow">self-evaluation</a>. On the <a href="https://tor.stackexchange.com/review/site-eval" rel="nofollow">evaluation page</a> you’ll see some questions and answers. Please go through this list and rate those questions. It helps the Q&amp;A site to improve those answers and see where weaknesses are.</p>

<p>User 2313265939 lives in a heavily censored region and wants an <a href="https://learn.adafruit.com/onion-pi/overview" rel="nofollow">OnionPi</a> to <a href="https://tor.stackexchange.com/q/6538/88" rel="nofollow">connect to the meek-amazon pluggable transport</a>. If you have an answer, please share it with this user.</p>

<h1>This week in Tor history</h1>

<p><a href="https://lists.torproject.org/pipermail/tor-news/2014-April/000039.html" rel="nofollow">A year ago this week</a>, Tor developers were discussing the possibility of distributing bridge relay addresses via QR code, to avoid tricky copy-pastes and input errors that might cause a failed connection.  Today, you can request some bridge lines from <a href="https://bridges.torproject.org/" rel="nofollow">BridgeDB</a> and select “Show QR code” to be shown…exactly that. Bridge address QR code recognition will soon <a href="https://lists.mayfirst.org/pipermail/guardian-dev/2015-March/004283.html" rel="nofollow">make its way</a> into the Orbot stable release, as well, so your simple censorship circumvention is no longer dependent on finicky touchscreen keyboards!</p>

<p>This issue of Tor Weekly News has been assembled by Harmony, Karsten Loesing, qbi, and the Tails team.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

