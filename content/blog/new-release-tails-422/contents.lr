title: New Release: Tails 4.22
---
pub_date: 2021-09-07
---
author: tails
---
tags:

tails
tails releases
anonymous operating system
---
categories:

partners
releases
---
_html_body:

<p>In Tails 4.22, we focused on solving the most important issues in the <em>Tor Connection</em> assistant to make it more robust and easier to use.</p>

<h1>Changes and updates</h1>

<h2>Included software and hardware support</h2>

<ul>
<li>
Update <em>Tor Browser</em> to <a href="https://blog.torproject.org/new-release-tor-browser-1056" rel="nofollow">10.5.6</a>.
</li>
<li>
Update <em>Thunderbird</em> to <a href="https://www.thunderbird.net/en-US/thunderbird/78.13.0/releasenotes/" rel="nofollow">78.13</a>.
</li>
<li>
Update the AMD graphics firmware to 20210818. This should improve the support for some AMD graphics cards.
</li>
</ul>

<h2><em>Tor Connection</em></h2>

<ul>
<li>
Change the custom bridge interface to only allow entering 1 bridge. (<a href="https://gitlab.tails.boum.org/tails/tails/-/issues/18550" rel="nofollow">#18550</a>)<br />
People had troubles knowing how to enter their custom bridges when the widget was a textarea and only the first bridge is used anyway.
</li>
<li>
Allow saving 1 custom bridge in the Persistent Storage. (<a href="https://gitlab.tails.boum.org/tails/tails/-/issues/5461" rel="nofollow">#5461</a>)
</li>
<li>
Allow fixing the clock manually when connecting to Tor using bridges fails. (<a href="https://gitlab.tails.boum.org/tails/tails/-/issues/15548" rel="nofollow">#15548</a>)<br />
This helps people East from London connect to Tor using obfs4 bridges and makes connecting to Tor more robust in general.
</li>
<li>
Reduce the timeout that determines whether we can connect to Tor at all from 30 seconds to 10 seconds. Increase the timeout to start Tor entirely from 120 seconds to 600 seconds. (<a href="https://gitlab.tails.boum.org/tails/tails/-/issues/18501" rel="nofollow">#18501</a>).<br />
<em>Tor Connection</em> now fails quicker when it's impossible to connect to Tor, while being more robust on slow Internet connections.
</li>
<li>
Allow trying again to connect to Tor from the error screen. (<a href="https://gitlab.tails.boum.org/tails/tails/-/issues/18539" rel="nofollow">#18539</a>)
</li>
</ul>

<h2><em>Unsafe Browser</em></h2>

<ul>
<li>
Stop restarting Tor when exiting the <em>Unsafe Browser</em>. (<a href="https://gitlab.tails.boum.org/tails/tails/-/issues/18562" rel="nofollow">#18562</a>)
</li>
<li>
Only mention the Persistent Storage in the <em>Unsafe Browser</em> warning when there is already a Persistent Storage. (<a href="https://gitlab.tails.boum.org/tails/tails/-/issues/18551" rel="nofollow">#18551</a>)
</li>
</ul>

<h2>Others</h2>

<ul>
<li>
Make sure that automatic upgrades are downloaded from a working mirror. (<a href="https://gitlab.tails.boum.org/tails/tails/-/issues/15755" rel="nofollow">#15755</a>)
</li>
<li>
Add Russian to the offline documentation included in Tails.
</li>
</ul>

<h1>Fixed problems</h1>

<h2><em>Tor Connection</em></h2>

<ul>
<li>
Fix connecting to Tor using the default bridges. (<a href="https://gitlab.tails.boum.org/tails/tails/-/issues/18462" rel="nofollow">#18462</a>)
</li>
<li>
Fix connecting to Tor when the Wi-Fi settings are saved in the Persistent Storage. (<a href="https://gitlab.tails.boum.org/tails/tails/-/issues/18532" rel="nofollow">#18532</a>)
</li>
<li>
Stop trying to connect to Tor in the background when <em>Tor Connection</em> reaches the error screen. (<a href="https://gitlab.tails.boum.org/tails/tails/-/issues/18740" rel="nofollow">#18740</a>)
</li>
</ul>

<p>For more details, read our <a href="https://gitlab.tails.boum.org/tails/tails/-/blob/master/debian/changelog" rel="nofollow">changelog</a>.</p>

<h1>Known issues</h1>

<p>None specific to this release.<br />
See the list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">long-standing issues</a>.</p>

<h1>Get Tails 4.22</h1>

<h2>To upgrade your Tails USB stick and keep your persistent storage</h2>

<ul>
<li>
Automatic upgrades are broken from Tails 4.14 and earlier.<br />
Follow our instructions to do an <a href="https://tails.boum.org/doc/upgrade/error/check/#4.18" rel="nofollow">automatic upgrade from Tails 4.15, Tails 4.16, Tails 4.17, or Tails 4.18</a>.
</li>
<li>
Automatic upgrades are available from Tails 4.19 or later to 4.22.<br />
You can <a href="https://tails.boum.org/doc/upgrade/#reduce" rel="nofollow">reduce the size of the download</a> of future automatic upgrades by doing a manual upgrade to the latest version.
</li>
<li>
If you cannot do an automatic upgrade or if Tails fails to start after an automatic upgrade, please try to do a <a href="https://tails.boum.org/doc/upgrade/#manual" rel="nofollow">manual upgrade</a>.
</li>
</ul>

<h2>To install Tails on a new USB stick</h2>

<p>Follow our installation instructions:</p>

<ul>
<li><a href="https://tails.boum.org/install/win/" rel="nofollow">Install from Windows</a></li>
<li><a href="https://tails.boum.org/install/mac/" rel="nofollow">Install from macOS</a></li>
<li><a href="https://tails.boum.org/install/linux/" rel="nofollow">Install from Linux</a></li>
</ul>

<p>The Persistent Storage on the USB stick will be lost if you install instead of upgrading.</p>

<h2>To download only</h2>

<p>If you don't need installation or upgrade instructions, you can download Tails 4.22 directly:</p>

<ul>
<li><a href="https://tails.boum.org/install/download/" rel="nofollow">For USB sticks (USB image)</a></li>
<li><a href="https://tails.boum.org/install/download-iso/" rel="nofollow">For DVDs and virtual machines (ISO image)</a></li>
</ul>

<h1>What's coming up?</h1>

<p>Tails 4.23 is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for October 5.<br />
Have a look at our <a href="https://tails.boum.org/contribute/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<h1>Support and feedback</h1>

<p>For support and feedback, visit the <a href="https://tails.boum.org/support/" rel="nofollow">Support section</a> on the Tails website.</p>

