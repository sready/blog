title: Tor 0.2.1.6-alpha Released
---
pub_date: 2008-10-15
---
author: phobos
---
tags:

bug fixes
alpha
---
categories: releases
---
_html_body:

<p>Tor 0.2.1.6-alpha further improves performance and robustness of hidden<br />
services, starts work on supporting per-country relay selection, and<br />
fixes a variety of smaller issues.</p>

<p>The original announcement can be found at<br />
 <a href="http://archives.seul.org/or/talk/Oct-2008/msg00093.html" rel="nofollow">http://archives.seul.org/or/talk/Oct-2008/msg00093.html</a></p>

<p>Changes in version 0.2.1.6-alpha - 2008-09-30</p>

<ul>
<li>Major features:</li>
<ul>
<li>Implement proposal 121: make it possible to build hidden services<br />
      that only certain clients are allowed to connect to. This is<br />
      enforced at several points, so that unauthorized clients are unable<br />
      to send INTRODUCE cells to the service, or even (depending on the<br />
      type of authentication) to learn introduction points. This feature<br />
      raises the bar for certain kinds of active attacks against hidden<br />
      services. Code by Karsten Loesing.</li>
<li>Relays now store and serve v2 hidden service descriptors by default,<br />
      i.e., the new default value for HidServDirectoryV2 is 1. This is<br />
      the last step in proposal 114, which aims to make hidden service<br />
      lookups more reliable.</li>
<li>Start work to allow node restrictions to include country codes. The<br />
      syntax to exclude nodes in a country with country code XX is<br />
      "ExcludeNodes {XX}". Patch from Robert Hogan. It still needs some<br />
      refinement to decide what config options should take priority if<br />
      you ask to both use a particular node and exclude it.</li>
<li>Allow ExitNodes list to include IP ranges and country codes, just<br />
      like the Exclude*Nodes lists. Patch from Robert Hogan.</li>
</ul>
<li>Major bugfixes:</li>
<ul>
<li>Fix a bug when parsing ports in tor_addr_port_parse() that caused<br />
      Tor to fail to start if you had it configured to use a bridge<br />
      relay. Fixes bug 809. Bugfix on 0.2.1.5-alpha.</li>
<li>When extending a circuit to a hidden service directory to upload a<br />
      rendezvous descriptor using a BEGIN_DIR cell, almost 1/6 of all<br />
      requests failed, because the router descriptor had not been<br />
      downloaded yet. In these cases, we now wait until the router<br />
      descriptor is downloaded, and then retry. Likewise, clients<br />
      now skip over a hidden service directory if they don't yet have<br />
      its router descriptor, rather than futilely requesting it and<br />
      putting mysterious complaints in the logs. Fixes bug 767. Bugfix<br />
      on 0.2.0.10-alpha.</li>
<li>When fetching v0 and v2 rendezvous service descriptors in parallel,<br />
      we were failing the whole hidden service request when the v0<br />
      descriptor fetch fails, even if the v2 fetch is still pending and<br />
      might succeed. Similarly, if the last v2 fetch fails, we were<br />
      failing the whole hidden service request even if a v0 fetch is<br />
      still pending. Fixes bug 814. Bugfix on 0.2.0.10-alpha.</li>
<li>DNS replies need to have names matching their requests, but<br />
      these names should be in the questions section, not necessarily<br />
      in the answers section. Fixes bug 823. Bugfix on 0.2.1.5-alpha.</li>
</ul>
<li>Minor features:</li>
<ul>
<li>Update to the "September 1 2008" ip-to-country file.<br />
    - Allow ports 465 and 587 in the default exit policy again. We had<br />
      rejected them in 0.1.0.15, because back in 2005 they were commonly<br />
      misconfigured and ended up as spam targets. We hear they are better<br />
      locked down these days.</li>
<li>Use a lockfile to make sure that two Tor processes are not<br />
      simultaneously running with the same datadir.</li>
<li>Serve the latest v3 networkstatus consensus via the control<br />
      port. Use "getinfo dir/status-vote/current/consensus" to fetch it.</li>
<li>Better logging about stability/reliability calculations on directory<br />
      servers.</li>
<li>Drop the requirement to have an open dir port for storing and<br />
      serving v2 hidden service descriptors.</li>
<li>Directory authorities now serve a /tor/dbg-stability.txt URL to<br />
      help debug WFU and MTBF calculations.</li>
<li>Implement most of Proposal 152: allow specialized servers to permit<br />
      single-hop circuits, and clients to use those servers to build<br />
      single-hop circuits when using a specialized controller. Patch<br />
      from Josh Albrecht. Resolves feature request 768.</li>
<li>Add a -p option to tor-resolve for specifying the SOCKS port: some<br />
      people find host:port too confusing.</li>
<li>Make TrackHostExit mappings expire a while after their last use, not<br />
      after their creation.  Patch from Robert Hogan.</li>
<li>Provide circuit purposes along with circuit events to the controller.</li>
</ul>
<li>Minor bugfixes:</li>
<ul>
<li>Fix compile on OpenBSD 4.4-current. Bugfix on 0.2.1.5-alpha.<br />
      Reported by Tas.</li>
<li>Fixed some memory leaks -- some quite frequent, some almost<br />
      impossible to trigger -- based on results from Coverity.</li>
<li>When testing for libevent functions, set the LDFLAGS variable<br />
      correctly. Found by Riastradh.</li>
<li>Fix an assertion bug in parsing policy-related options; possible fix<br />
      for bug 811.</li>
<li>Catch and report a few more bootstrapping failure cases when Tor<br />
      fails to establish a TCP connection. Cleanup on 0.2.1.x.</li>
<li>Avoid a bug where the FastFirstHopPK 0 option would keep Tor from<br />
      bootstrapping with tunneled directory connections. Bugfix on<br />
      0.1.2.5-alpha. Fixes bug 797. Found by Erwin Lam.</li>
<li>When asked to connect to A.B.exit:80, if we don't know the IP for A<br />
      and we know that server B rejects most-but-not all connections to<br />
      port 80, we would previously reject the connection. Now, we assume<br />
      the user knows what they were asking for. Fixes bug 752. Bugfix<br />
      on 0.0.9rc5. Diagnosed by BarkerJr.</li>
<li>If we are not using BEGIN_DIR cells, don't attempt to contact hidden<br />
      service directories if they have no advertised dir port. Bugfix<br />
      on 0.2.0.10-alpha.</li>
<li>If we overrun our per-second write limits a little, count this as<br />
      having used up our write allocation for the second, and choke<br />
      outgoing directory writes. Previously, we had only counted this when<br />
      we had met our limits precisely. Fixes bug 824. Patch from by rovv.<br />
      Bugfix on 0.2.0.x (??).</li>
<li>Avoid a "0 divided by 0" calculation when calculating router uptime<br />
      at directory authorities. Bugfix on 0.2.0.8-alpha.</li>
<li>Make DNS resolved controller events into "CLOSED", not<br />
      "FAILED". Bugfix on 0.1.2.5-alpha. Fix by Robert Hogan. Resolves<br />
      bug 807.</li>
<li>Fix a bug where an unreachable relay would establish enough<br />
      reachability testing circuits to do a bandwidth test -- if<br />
      we already have a connection to the middle hop of the testing<br />
      circuit, then it could establish the last hop by using the existing<br />
      connection. Bugfix on 0.1.2.2-alpha, exposed when we made testing<br />
      circuits no longer use entry guards in 0.2.1.3-alpha.</li>
<li>If we have correct permissions on $datadir, we complain to stdout<br />
      and fail to start. But dangerous permissions on<br />
      $datadir/cached-status/ would cause us to open a log and complain<br />
      there. Now complain to stdout and fail to start in both cases. Fixes<br />
      bug 820, reported by seeess.</li>
<li>Remove the old v2 directory authority 'lefkada' from the default<br />
      list. It has been gone for many months.</li>
</ul>
<li>Code simplifications and refactoring:</li>
<ul>
<li>Revise the connection_new functions so that a more typesafe variant<br />
      exists. This will work better with Coverity, and let us find any<br />
      actual mistakes we're making here.</li>
<li>Refactor unit testing logic so that dmalloc can be used sensibly<br />
      with unit tests to check for memory leaks.</li>
<li>Move all hidden-service related fields from connection and circuit<br />
      structure to substructures: this way they won't eat so much memory.</li>
</ul>
</ul>

---
_comments:

<a id="comment-293"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 16, 2008</p>
    </div>
    <a href="#comment-293">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293" class="permalink" rel="bookmark">Firefox writing cache to disk</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If I downloaded tor button, will it prevent firefox from writing cache to my disk? If not, how do I disable it?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-295"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-295" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">October 16, 2008</p>
    </div>
    <a href="#comment-295">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-295" class="permalink" rel="bookmark">By default, yes it will.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>By default, yes it will.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-607"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-607" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 08, 2009</p>
    </div>
    <a href="#comment-607">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-607" class="permalink" rel="bookmark">ExitNodes {XX} can&#039;t work?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I put the following command in the config file but can't work..</p>
<p>ExitNodes {hk}, {us}, {jp}<br />
StrictExitNodes 1</p>
<p>anything wrong</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1085"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1085" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 02, 2009</p>
    </div>
    <a href="#comment-1085">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1085" class="permalink" rel="bookmark">always the same servers</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>the tor is changing only between 10 different servers, most time all are in the same town. why ist that? should they not change between 1k or so?<br />
it's that, i'm in germany and it only switches between german servers too. the providers are forced to log all traffic. what is the sense of using tor then? i'm running the stable version and can not exclude countries.</p>
</div>
  </div>
</article>
<!-- Comment END -->
