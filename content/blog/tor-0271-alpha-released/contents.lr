title: Tor 0.2.7.1-alpha is released
---
pub_date: 2015-05-12
---
author: nickm
---
tags:

tor
alpha
release
---
categories:

network
releases
---
_html_body:

<p>Tor 0.2.7.1-alpha is the first alpha release in its series. It includes numerous small features and bugfixes against previous Tor versions, and numerous small infrastructure improvements. The most notable features are several new ways for controllers to interact with the hidden services subsystem. </p>

<p>You can download the source from the usual place on the website. Packages should be up in a few days.</p>

<p>NOTE: This is an alpha release. Please expect bugs.</p>

<h2>Changes in version 0.2.7.1-alpha - 2015-05-12</h2>

<ul>
<li>New system requirements:
<ul>
<li>Tor no longer includes workarounds to support Libevent versions before 1.3e. Libevent 2.0 or later is recommended. Closes ticket 15248.
  </li>
</ul>
</li>
<li>Major features (controller):
<ul>
<li>Add the ADD_ONION and DEL_ONION commands that allow the creation and management of hidden services via the controller. Closes ticket 6411.
  </li>
<li>New "GETINFO onions/current" and "GETINFO onions/detached" commands to get information about hidden services created via the controller. Part of ticket 6411.
  </li>
<li>New HSFETCH command to launch a request for a hidden service descriptor. Closes ticket 14847.
  </li>
<li>New HSPOST command to upload a hidden service descriptor. Closes ticket 3523. Patch by "DonnchaC".
  </li>
</ul>
</li>
</ul>

<p> </p>

<ul>
<li>Major bugfixes (hidden services):
<ul>
<li>Revert commit that made directory authorities assign the HSDir flag to relay without a DirPort; this was bad because such relays can't handle BEGIN_DIR cells. Fixes bug 15850; bugfix on tor-0.2.6.3-alpha.
  </li>
</ul>
</li>
<li>Minor features (clock-jump tolerance):
<ul>
<li>Recover better when our clock jumps back many hours, like might happen for Tails or Whonix users who start with a very wrong hardware clock, use Tor to discover a more accurate time, and then fix their clock. Resolves part of ticket 8766.
  </li>
</ul>
</li>
<li>Minor features (command-line interface):
<ul>
<li>Make --hash-password imply --hush to prevent unnecessary noise. Closes ticket 15542. Patch from "cypherpunks".
  </li>
<li>Print a warning whenever we find a relative file path being used as torrc option. Resolves issue 14018.
  </li>
</ul>
</li>
<li>Minor features (controller):
<ul>
<li>Add DirAuthority lines for default directory authorities to the output of the "GETINFO config/defaults" command if not already present. Implements ticket 14840.
  </li>
<li>Controllers can now use "GETINFO hs/client/desc/id/..." to retrieve items from the client's hidden service descriptor cache. Closes ticket 14845.
  </li>
<li>Implement a new controller command "GETINFO status/fresh-relay- descs" to fetch a descriptor/extrainfo pair that was generated on demand just for the controller's use. Implements ticket 14784.
  </li>
</ul>
</li>
<li>Minor features (DoS-resistance):
<ul>
<li>Make it harder for attackers to overload hidden services with introductions, by blocking multiple introduction requests on the same circuit. Resolves ticket 15515.
  </li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip to the April 8 2015 Maxmind GeoLite2 Country database.
  </li>
<li>Update geoip6 to the April 8 2015 Maxmind GeoLite2 Country database.
  </li>
</ul>
</li>
<li>Minor features (HS popularity countermeasure):
<ul>
<li>To avoid leaking HS popularity, don't cycle the introduction point when we've handled a fixed number of INTRODUCE2 cells but instead cycle it when a random number of introductions is reached, thus making it more difficult for an attacker to find out the amount of clients that have used the introduction point for a specific HS. Closes ticket 15745.
  </li>
</ul>
</li>
<li>Minor features (logging):
<ul>
<li>Include the Tor version in all LD_BUG log messages, since people tend to cut and paste those into the bugtracker. Implements ticket 15026.
  </li>
</ul>
</li>
<li>Minor features (pluggable transports):
<ul>
<li>When launching managed pluggable transports on Linux systems, attempt to have the kernel deliver a SIGTERM on tor exit if the pluggable transport process is still running. Resolves ticket 15471.
  </li>
<li>When launching managed pluggable transports, setup a valid open stdin in the child process that can be used to detect if tor has terminated. The "TOR_PT_EXIT_ON_STDIN_CLOSE" environment variable can be used by implementations to detect this new behavior. Resolves ticket 15435.
  </li>
</ul>
</li>
<li>Minor features (testing):
<ul>
<li>Add a test to verify that the compiler does not eliminate our memwipe() implementation. Closes ticket 15377.
  </li>
<li>Add make rule `check-changes` to verify the format of changes files. Closes ticket 15180.
  </li>
<li>Add unit tests for control_event_is_interesting(). Add a compile- time check that the number of events doesn't exceed the capacity of control_event_t.event_mask. Closes ticket 15431, checks for bugs similar to 13085. Patch by "teor".
  </li>
<li>Command-line argument tests moved to Stem. Resolves ticket 14806.
  </li>
<li>Integrate the ntor, backtrace, and zero-length keys tests into the automake test suite. Closes ticket 15344.
  </li>
<li>Remove assertions during builds to determine Tor's test coverage. We don't want to trigger these even in assertions, so including them artificially makes our branch coverage look worse than it is. This patch provides the new test-stem-full and coverage-html-full configure options. Implements ticket 15400.
  </li>
</ul>
</li>
<li>Minor bugfixes (build):
<ul>
<li>Improve out-of-tree builds by making non-standard rules work and clean up additional files and directories. Fixes bug 15053; bugfix on 0.2.7.0-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (command-line interface):
<ul>
<li>When "--quiet" is provided along with "--validate-config", do not write anything to stdout on success. Fixes bug 14994; bugfix on 0.2.3.3-alpha.
  </li>
<li>When complaining about bad arguments to "--dump-config", use stderr, not stdout.
  </li>
</ul>
</li>
<li>Minor bugfixes (configuration, unit tests):
<ul>
<li>Only add the default fallback directories when the DirAuthorities, AlternateDirAuthority, and FallbackDir directory config options are set to their defaults. The default fallback directory list is currently empty, this fix will only change tor's behavior when it has default fallback directories. Includes unit tests for consider_adding_dir_servers(). Fixes bug 15642; bugfix on 90f6071d8dc0 in 0.2.4.7-alpha. Patch by "teor".
  </li>
</ul>
</li>
<li>Minor bugfixes (correctness):
<ul>
<li>For correctness, avoid modifying a constant string in handle_control_postdescriptor. Fixes bug 15546; bugfix on 0.1.1.16-rc.
  </li>
<li>Remove side-effects from tor_assert() calls. This was harmless, because we never disable assertions, but it is bad style and unnecessary. Fixes bug 15211; bugfix on 0.2.5.5, 0.2.2.36, and 0.2.0.10.
  </li>
</ul>
</li>
<li>Minor bugfixes (hidden service):
<ul>
<li>Fix an out-of-bounds read when parsing invalid INTRODUCE2 cells on a client authorized hidden service. Fixes bug 15823; bugfix on 0.2.1.6-alpha.
  </li>
<li>Remove an extraneous newline character from the end of hidden service descriptors. Fixes bug 15296; bugfix on 0.2.0.10-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (interface):
<ul>
<li>Print usage information for --dump-config when it is used without an argument. Also, fix the error message to use different wording and add newline at the end. Fixes bug 15541; bugfix on 0.2.5.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (logs):
<ul>
<li>When building Tor under Clang, do not include an extra set of parentheses in log messages that include function names. Fixes bug 15269; bugfix on every released version of Tor when compiled with recent enough Clang.
  </li>
</ul>
</li>
<li>Minor bugfixes (network):
<ul>
<li>When attempting to use fallback technique for network interface lookup, disregard loopback and multicast addresses since they are unsuitable for public communications.
  </li>
</ul>
</li>
<li>Minor bugfixes (statistics):
<ul>
<li>Disregard the ConnDirectionStatistics torrc options when Tor is not a relay since in that mode of operation no sensible data is being collected and because Tor might run into measurement hiccups when running as a client for some time, then becoming a relay. Fixes bug 15604; bugfix on 0.2.2.35.
  </li>
</ul>
</li>
<li>Minor bugfixes (test networks):
<ul>
<li>When self-testing reachability, use ExtendAllowPrivateAddresses to determine if local/private addresses imply reachability. The previous fix used TestingTorNetwork, which implies ExtendAllowPrivateAddresses, but this excluded rare configurations where ExtendAllowPrivateAddresses is set but TestingTorNetwork is not. Fixes bug 15771; bugfix on 0.2.6.1-alpha. Patch by "teor", issue discovered by CJ Ess.
  </li>
</ul>
</li>
<li>Minor bugfixes (testing):
<ul>
<li>Check for matching value in server response in ntor_ref.py. Fixes bug 15591; bugfix on 0.2.4.8-alpha. Reported and fixed by "joelanders".
  </li>
<li>Set the severity correctly when testing get_interface_addresses_ifaddrs() and get_interface_addresses_win32(), so that the tests fail gracefully instead of triggering an assertion. Fixes bug 15759; bugfix on 0.2.6.3-alpha. Reported by Nicolas Derive.
  </li>
</ul>
</li>
<li>Code simplification and refactoring:
<ul>
<li>Move the hacky fallback code out of get_interface_address6() into separate function and get it covered with unit-tests. Resolves ticket 14710.
  </li>
<li>Refactor hidden service client-side cache lookup to intelligently report its various failure cases, and disentangle failure cases involving a lack of introduction points. Closes ticket 14391.
  </li>
<li>Use our own Base64 encoder instead of OpenSSL's, to allow more control over the output. Part of ticket 15652.
  </li>
</ul>
</li>
<li>Documentation:
<ul>
<li>Improve the descriptions of statistics-related torrc options in the manpage to describe rationale and possible uses cases. Fixes issue 15550.
  </li>
<li>Improve the layout and formatting of ./configure --help messages. Closes ticket 15024. Patch from "cypherpunks".
  </li>
<li>Standardize on the term "server descriptor" in the manual page. Previously, we had used "router descriptor", "server descriptor", and "relay descriptor" interchangeably. Part of ticket 14987.
  </li>
</ul>
</li>
<li>Removed code:
<ul>
<li>Remove `USE_OPENSSL_BASE64` and the corresponding fallback code and always use the internal Base64 decoder. The internal decoder has been part of tor since tor-0.2.0.10-alpha, and no one should be using the OpenSSL one. Part of ticket 15652.
  </li>
<li>Remove the 'tor_strclear()' function; use memwipe() instead. Closes ticket 14922.
  </li>
</ul>
</li>
<li>Removed features:
<ul>
<li>Remove the (seldom-used) DynamicDHGroups feature. For anti- fingerprinting we now recommend pluggable transports; for forward- secrecy in TLS, we now use the P-256 group. Closes ticket 13736.
  </li>
<li>Remove the undocumented "--digests" command-line option. It complicated our build process, caused subtle build issues on multiple platforms, and is now redundant since we started including git version identifiers. Closes ticket 14742.
  </li>
<li>Tor no longer contains checks for ancient directory cache versions that didn't know about microdescriptors.
  </li>
<li>Tor no longer contains workarounds for stat files generated by super-old versions of Tor that didn't choose guards sensibly.
  </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-93251"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-93251" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 12, 2015</p>
    </div>
    <a href="#comment-93251">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-93251" class="permalink" rel="bookmark">According to Tails&#039;, We</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>According to Tails', <cite>We disabled in Tails the new circuit view of Tor Browser 4.5 for security reasons. You can still use the network map of Vidalia to inspect your circuits.</cite></p>
<p>If Tails' developers are correct, why do Tor developers not disable it in the Tor Browser Bundle 4.5 and 4.5.1?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-93460"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-93460" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 14, 2015</p>
    </div>
    <a href="#comment-93460">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-93460" class="permalink" rel="bookmark">I like change #6411. Using</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I like change #6411. Using it already. Already looking forward to the day proposal 224 gets implemented, I see a lot of potential there!</p>
<p>On a different note, fervently hoping for the introduction of a layer of post-quantum cryptography into Tor. Apparently no one can build a useful quantum computer yet but probably we won't know till it's too late ... and then I want my decrypted comms to be as old as possible.</p>
<p>As always, keep up the good work.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-93482"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-93482" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  yawning
  </article>
    <div class="comment-header">
      <p class="comment__submitted">yawning said:</p>
      <p class="date-time">May 14, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-93460" class="permalink" rel="bookmark">I like change #6411. Using</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-93482">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-93482" class="permalink" rel="bookmark">Glad you like #6411.
I&#039;ve</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Glad you like #6411.</p>
<p>I've given some thought to the PQ crypto thing on and off (See basket in my github for something that does PQ crypto to a bridge for example).  There's a good number of open research questions here, and at least one rather large engineering problem that needs to be solved before this is possible in the core Tor protocol.</p>
<p>The research questions:</p>
<ol>
<li>What key exchange primitive?  I'm inclined to say, the approach I made in basket (that the NTRU people and a bunch of academics also used for a paper) is probably the safest (client sends a Curve25519 public key + NTRUEncrypt public key, server sends the response back in a NTRUEncrypt-ed ciphertext).  Ring-LWE seems to be popular as well, but is new and scary, and SIDH is very scary (Solving this will get PQ forward secrecy, assuming the primitive chosen is sound).
</li>
<li>Ideally it would also be cool to have PQ signatures used for certain things.  The only signature algorithm I know of that isn't absolutely ridiculous and impractical would be SPHINCS256, but the keys and signatures are still gigantic relative to classical primitives, and it's kind of slow without AVX2.
</li>
<li>CTR-AES128 probably needs to go as well.  There's a bunch of "relevant today" type attacks that should be addressed when this is replaced, but solving that requires more crypto research (LIONESS was too slow, sad times).
</li>
</ol>
<p>The engineering problem:</p>
<ol>
<li>The cells used in the tor protocol level handshake would need to support fragmented payloads, because the keys involved won't fit in a single cell.  Not insurmountable, but tricky.
</li>
</ol>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-93674"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-93674" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 16, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-93482" class="permalink" rel="bookmark">Glad you like #6411.
I&#039;ve</a> by yawning</p>
    <a href="#comment-93674">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-93674" class="permalink" rel="bookmark">Hi, thanks for the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi, thanks for the braindump.</p>
<p>I see there is still a lot of groundwork to be done. Was thinking along the lines of Bos, Costello, Naehrig and Stebila and their experiment with a hybrid ciphersuite for TLS that wraps Ring-LWE inside ECDH, such that security should be at least as good as the non-PQ part even if RLWE fails expectations.</p>
<p><a href="https://github.com/dstebila/openssl-rlwekex/" rel="nofollow">https://github.com/dstebila/openssl-rlwekex/</a> (OpenSSL_1_0_1-stable branch) + conference paper</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-93789"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-93789" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 18, 2015</p>
    </div>
    <a href="#comment-93789">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-93789" class="permalink" rel="bookmark">Will we be able to host</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Will we be able to host hidden services with 0.2.7 without holding privatekey on HDD?</p>
<p>It would be nice if we could have TTL on a hidden service so we could run traffic through a cloud for DDOS protection.</p>
<p>People with botnet can do horror stuff to a hidden service at the moment.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-93817"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-93817" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  yawning
  </article>
    <div class="comment-header">
      <p class="comment__submitted">yawning said:</p>
      <p class="date-time">May 19, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-93789" class="permalink" rel="bookmark">Will we be able to host</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-93817">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-93817" class="permalink" rel="bookmark">&gt; Will we be able to host</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; Will we be able to host hidden services with 0.2.7 without holding privatekey on HDD?</p>
<p>Yes.  The new 'ADD_ONION' command supports supplying an existing private key, or having tor generate one (and optionally discarding it to enable one-shot services).  In either case, tor will not save the private key anywhere, though the standard caveats of "I hope you are using encrypted swap, and have disabled core dumps" apply.</p>
<p>&gt; People with botnet can do horror stuff to a hidden service at the moment.</p>
<p>People with a botnet can do lots of nasty things to just about anything at the moment.  I have some ideas for potential mitigations here, though I think some of this really is an application level problem, and most of the reasonably easy solutions to this sort of problem just force the adversary to get a bigger bot net.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-94113"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94113" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 26, 2015</p>
    </div>
    <a href="#comment-94113">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94113" class="permalink" rel="bookmark">So excited to see what</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>So excited to see what people can create with #6411</p>
</div>
  </div>
</article>
<!-- Comment END -->
