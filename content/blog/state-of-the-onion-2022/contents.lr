title: You’re Invited: State of the Onion 2022
---
pub_date: 2022-11-04
---
author: isabela
---
categories:

community
---
summary: We will be hosting another State of the Onion livestream, a compilation of updates from the Tor Project's different teams discussing  highlights of their work during the year and what we are excited about in the upcoming year, on November 9th from 17:00 - 19:00 UTC.
---
body:

It is time for another State of the Onion! This is our annual virtual event where we share updates from Tor Project’s different teams, highlighting their work during the year and what we are excited for the upcoming year. We also have a part of the stream that is dedicated to our community for them to present their amazing work. You can check out our previous State of the Onion streams ([2020](https://www.youtube.com/watch?v=IyWyTypRGWQ) and [2021](https://www.youtube.com/watch?v=mNhIjtXuVzk)) at our YouTube channel.

This year we will be doing things a bit differently than we have in the past. Instead of having all the presentations happening on the same day, we are organizing two streams on two different days, one for Tor Project’s teams and another for our community. So please make sure to save the date for both events!

- **Wednesday, Nov 9, 17:00 - 19:00 UTC => [State of the Onion with the Tor Project’s teams presenting](https://www.youtube.com/watch?v=uSyBZ7GIzJY).**
- **Wednesday Nov 16, 17:00 - 19:00 UTC => [State of the Onion with Tor’s Community presenting](https://www.youtube.com/watch?v=O-7k0PjnBbk).**

The State of the Onion will be streamed over our [YouTube](https://www.youtube.com/c/TorProjectInc) account.

Join the conversation on social media using the hashtag: #StateOfTheOnion2022 or post your questions and comments in the YouTube chat.

## Nov 9 Program - State of the Onion: The Tor Project’s teams.

| Topic | Speaker | Details |
|-------|---------|---------|
| Opening | Isabela, Executive Director | Host presenting opening remarks. |
| Community Team | Gus, Community Team Lead | User support, translations, digital security trainings, relay operators organizing, are just some of the things that our Community Team will be sharing. |
| Censorship Circumvention | Meskio, Anti-Censorship Team Lead | Advances on Anti-Censorship at Tor. How are we reacting to the attempts to block Tor and what are we doing to be prepared for the next ones that will come. |
| Onions Everywhere | Raya, Education and communities coordinator | As part of the Community Team, we started an initiative to provide tools and support for organizations adopting onion services. We will talk about this work and the tools we have created as well as our plans to continue it in 2023. |
| UX & Design Team | Duncan, UX & Design Team Lead | A recap of recent work from the team in 2022, including our ethical user research program, product updates and design highlights – plus some shiny things we have up our sleeve for next year. |
| Applications & Tor Browser | Richard, Applications Team Lead | The Tor Browser 12.0 Release and Looking to 12.5 |
| VPN Project | Micah, Director of Engineering | Meet our new Director of Engineering! We will share update on the progress on our VPN project. |
| Break | Isabela | Why is supporting Tor important? |
| Network Health | Georg, Network Health Lead | Highlights of the year and upcoming projects. |
| Tor network, little “t” tor | Alex, Network Team Lead | Highlights of the year and upcoming projects. |
| Arti | Nick M., Network Team and Tor Project co-founder | Updates on Arti, a new Tor client implementation in Rust. |
| Metrics | Hiro, Metrics Lead | Highlights of the year and upcoming projects. |
| Shadow | Jim Newsome, Shadow Simulation Developer | Intro to Shadow and the tech advancements we've made, the "push button simulation" setup, and how we're using it to evaluate changes to tor. |
| TPA | Anarcat, Tor Project Admins Team Lead | A walk through sysadmin magic land: who we are, what we do, what we did last year, and what's coming. |
| Year End Fundraising | Al, Fundraising Director | The Tor Project is a 501(c)(3) nonprofit--let's talk about how you can power privacy online with your support! |
| Closing | Isabela | Closing remarks. |

## Nov 16 Program - State of the Onion for the Tor community.

| Topic | Speaker | Details |
|-------|---------|---------|
| Opening | Gaba, Tor Project’s Project Manager | Host presenting opening remarks. |
| [OONI - Open Observatory of Network Interference](https://ooni.org/) | Maria Xynou | OONI will share highlights from 2022, as well as upcoming plans for 2023. |
| [Guardian Project](https://guardianproject.info/) | Nathan Freitas and Fabiola Maurice | We will share updates on all the different apps and mobile work we do: Orbot, Onion Browser, OnionShare mobile and  mobile developer support, integration, SDKs. And share about our work with the Latin America community. |
| [Calyx Institute](https://calyxinstitute.org/) | Nick Merrill | Calyx Institute will present about the progress on Onionshare for Android and work on integrating Tor into CalyxOS. |
| [Superbloom](https://simplysecure.org/) | Susan & Ngoc | Simply Secure is rebranding as  Superbloom. Superbloom leverages design as a transformative practice to shift power in the tech ecosystem, because everyone deserves technology they can trust. |
| [Ricochet Refresh](https://www.ricochetrefresh.net/) and [Gosling](https://github.com/blueprint-freespeech/gosling) | Richard, Engineer | Presentation on updates to Ricochet-Refresh and Gosling from this year. |
| [Quiet](https://www.tryquiet.org/) | Holmes Wilson | Quiet is a peer-to-peer Slack alternative for desktop and mobile built on Tor and IPFS, which lets communities control their own data without running their own servers. We'll describe how Quiet works and demo group chat, file transfer, GIFs, and our fully-p2p Android app. |
| [Privacy Accelerator](https://privacyaccelerator.org/) and [DemHack](https://demhack.ru/) | Olga Nemirovskaya | Privacy Accelerator is an online accelerator for commercial and non-commercial projects in the field of privacy, access to information and legal tech. DemHack is a hackathon event done in collaboration with Tor and other members of the community. |
| [Roskomsvoboda](https://roskomsvoboda.org/) | Sarkis Darbinyan | Talk about how the trial over blocking Tor in Russia was conducted, as well as the current situation concerning Tor in Russia. |
| [Tails](https://tails.boum.org/) | Intrigeri and Boyska | Tails is a portable operating system that protects against surveillance in censorship. |
| Closing | Gaba, Tor Project’s Project Manager | Host presenting closing remarks. |
