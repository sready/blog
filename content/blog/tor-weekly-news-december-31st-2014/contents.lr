title: Tor Weekly News — December 31st, 2014
---
pub_date: 2014-12-31
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the final issue in 2014 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Attacks and rumors of attacks</h1>

<p>Two weeks ago, the Tor Project relayed a <a href="https://blog.torproject.org/blog/possible-upcoming-attempts-disable-tor-network" rel="nofollow">warning</a> from an unspecified source to the effect that someone may have been preparing to seize, attack, or otherwise disable one or more of Tor’s <a href="https://metrics.torproject.org/about.html#directory-authority" rel="nofollow">directory authorities</a> in a bid to disrupt the entire Tor network. The lack of any specific information about the threat caused understandable concern in the Tor community, and several events that followed over the next fortnight did little to dispel this.</p>

<p>First, the operator of a large Tor exit relay cluster <a href="https://lists.torproject.org/pipermail/tor-talk/2014-December/036067.html" rel="nofollow">reported</a> that his servers may have been physically interfered with by unknown parties a short while before his message. Later <a href="https://lists.torproject.org/pipermail/tor-talk/2014-December/036084.html" rel="nofollow">updates</a> suggested that foul play was less likely than initially thought.</p>

<p>Several days later, a large number of small exit relays were created all at once, in what appeared to be a “<a href="https://en.wikipedia.org/wiki/Sybil_attack" rel="nofollow">Sybil attack</a>”; this was <a href="https://lists.torproject.org/pipermail/tor-consensus-health/2014-December/005381.html" rel="nofollow">detected</a> and halted almost immediately, as was a <a href="https://lists.torproject.org/pipermail/tor-consensus-health/2014-December/005414.html" rel="nofollow">second, more recent incident</a>. As the Tor Project put it in a <a href="http://www.twitlonger.com/show/n_1sjg365" rel="nofollow">response</a>, “we don’t expect any anonymity or performance effects based on what we've seen so far”, although a side-effect of the countermeasure is that relays hosted on some IP ranges are currently being <a href="https://lists.torproject.org/pipermail/tor-relays/2014-December/006020.html" rel="nofollow">rejected</a> by dirauths.</p>

<p>As far as anyone can tell, these events are not related in any way to the initial warning. The Tor network has functioned normally throughout this period, and the appearance of a series of incidents is likely to be the result of coincidence (helped by the online rumor mill) rather than a coordinated campaign. It is never possible to say with certainty that attacks on the network will not occur, but the threat referred to in the original blog post has not yet materialized — and “no news is good news”.</p>

<h1>Miscellaneous news</h1>

<p>Lasse Øverlier discovered that <a href="http://www.cs.kau.se/philwint/scramblesuit/" rel="nofollow">ScrambleSuit</a>’s protection against “replay attacks”, in which an adversary repeats a client authentication event to learn that the server is in fact a ScrambleSuit bridge, doesn’t work. Philipp Winter <a href="https://lists.torproject.org/pipermail/tor-dev/2014-December/008019.html" rel="nofollow">explained</a> the issue, and suggested some simple fixes.</p>

<p>Tom van der Woerdt asked for <a href="https://lists.torproject.org/pipermail/tor-dev/2014-December/008023.html" rel="nofollow">review</a> of a <a href="https://github.com/TvdW/tor/commit/75b5d94eb976ee4998189dc69582c62511dde9eb" rel="nofollow">patch</a> to remove the obsolete version 1 of Tor’s link protocol from the current software: “It’s a rather large patch, though not as large as the patch that will remove v2 of the protocol. However, before I write that one, can someone please check whether my patch is sane and I’m not violating any standards or policies?”</p>

<p>David Fifield <a href="https://bugs.torproject.org/12778#comment:5" rel="nofollow">trimmed</a> the length of <a href="https://trac.torproject.org/projects/tor/wiki/doc/meek" rel="nofollow">meek</a>’s HTTP headers from 413 to 162 bytes, reducing the bandwidth it uses by “approximately” 3%.</p>

<p>Thanks to <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-December/000815.html" rel="nofollow">Kura</a> for running a mirror of the Tor Project website and software archive!</p>

<p>This issue of Tor Weekly News has been assembled by Harmony, David Fifield, Chuck Peters, and Roger Dingledine.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

