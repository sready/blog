title: New Release: Tor Browser 11.0.11 (Windows, macOS, Linux)
---
pub_date: 2022-05-03
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 11.0.11 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.0.11 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.0.11/).

This version includes important security updates to Firefox:
- https://www.mozilla.org/en-US/security/advisories/mfsa2022-17/

Tor Browser 11.0.11 updates Firefox on Windows, macOS, and Linux to 91.9.0esr.

We use the opportunity as well to update various other components of Tor Browser:
- NoScript 11.4.5

The full changelog since [Tor Browser 11.0.10](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-11.0) is:

- Windows + OS X + Linux
  - Update Firefox to 91.9.0esr
  - Update NoScript to 11.4.5
  - [Bug tor-browser#21484](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/21484): Remove or hide "What's New" link from About dialog
  - [Bug tor-browser#34366](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/34366): The onion-location mechanism does not redirect to full URL
  - [Bug tor-browser-build#40482](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40482): Remove smallerrichard builtin bridge
- Build System
  - Windows + OS X + Linux
    - Update Go to 1.17.9

