title: Tor Meetup (Brussels)
---
author: steph
---
start_date: 2019-11-08
---
body:

![tor-meetup-brussels](/sites/default/files/inline-images/tor-brussels-meetup.png)

Dear Tor friends and relay operators,

Reclaiming your privacy on the internet may seem to be an overwhelming task. You aren’t powerless, however. On Friday (08.11.2019) a Tor meetup will take place at Mundo B in Brussels, Belgium to have a chat on how Tor can help. Join us to learn about how Tor works and how it can be used to enhance your personal privacy on the internet. We will discuss how Tor achieves a high degree of privacy for its users, how to use Tor Browser, and other uses of Tor for enhancing privacy.

The invitation is for all, independent of how much one knows about Tor. It will happen just before the open-to-all, weekend-long privacy unconference [Freedom Not Fear](https://freedomnotfear.org). Feel free to stick around for its [opening keynote](https://www.freedomnotfear.org/fnf-2019/opening-keynote-by-wojciech-wiewiorowski-edps) which starts just after the meetup. Hope to see many of you there.

When: Friday, November 8th, 2019

Time: 18:00 (6pm)

Where: Mundo B, Rue d'Edimbourg 26, Brussels, Belgium (in a room upstairs)

OSM: <https://www.openstreetmap.org/way/139078492#map=17/50.83787/4.36425>

